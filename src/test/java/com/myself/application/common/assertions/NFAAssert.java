package com.myself.application.common.assertions;

import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;
import org.assertj.core.api.AbstractAssert;

import java.util.Collection;
import java.util.Objects;

public class NFAAssert extends AbstractAssert<NFAAssert, NFA<State, String>> {

    private NFAAssert(NFA<State, String> actual) {
        super(actual, NFAAssert.class);
    }

    public static NFAAssert assertThat(NFA<State, String> actual) {
        return new NFAAssert(actual);
    }

    public NFAAssert hasTransition(String from, String symbol, String to) {
        isNotNull();

        if (!containsTransition(from, to, symbol)) {
            failWithMessage("Expected finite automata to have transition from <%s> to <%s> on symbol <%s>.",
                            from,
                            to,
                            symbol);
        }
        return this;
    }

    public NFAAssert hasState(String stateName) {
        isNotNull();

        if (!containsState(actual.getStates(), stateName)) {
            failWithMessage("Expected finite automata to have state <%s> but had <%s>.",
                            stateName,
                            actual.getStartState().getName());
        }
        return this;
    }

    public NFAAssert hasAcceptState(String stateName) {
        isNotNull();

        if (!containsState(actual.getAcceptStates(), stateName)) {
            failWithMessage("Expected finite automata to have accept state <%s> but had <%s>.",
                            stateName,
                            actual.getStartState().getName());
        }
        return this;
    }

    public NFAAssert hasStartState(String stateName) {
        isNotNull();

        if (!Objects.equals(actual.getStartState().getName(), stateName)) {
            failWithMessage("Expected finite automata to have start state <%s> but had <%s>.",
                            stateName,
                            actual.getStartState().getName());
        }
        return this;
    }

    public NFAAssert hasStatesSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getStates().size(), size)) {
            failWithMessage("Expected finite automata to have states size <%s> but had <%s>.",
                            size,
                            actual.getStates().size());
        }
        return this;
    }

    public NFAAssert hasAcceptStatesSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getAcceptStates().size(), size)) {
            failWithMessage("Expected finite automata to have accept states size <%s> but had <%s>.",
                            size,
                            actual.getAcceptStates().size());
        }
        return this;
    }

    public NFAAssert hasTransitionsSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getTransitions().size(), size)) {
            failWithMessage("Expected finite automata to have transitions size <%s> but had <%s>.",
                            size,
                            actual.getTransitions().size());
        }
        return this;
    }

    private boolean containsState(Collection<State> states, String stateName) {
        return states.stream()
                     .anyMatch(state -> state.getName().equals(stateName));
    }

    private boolean containsTransition(String from, String to, String symbol) {
        return actual.getTransitions()
                     .stream()
                     .anyMatch(transition ->
                                       transition.getFrom().getName().equals(from) &&
                                       transition.getTo().getName().equals(to) &&
                                       transition.getSymbol().equals(symbol));
    }

}
