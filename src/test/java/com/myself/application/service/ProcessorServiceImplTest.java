package com.myself.application.service;

import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.mapping.impl.DefaultExpressionMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import com.myself.application.service.impl.ProcessorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.common.assertions.ProcessorResultAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class ProcessorServiceImplTest {

    private ProcessorService service;

    @BeforeEach
    void setUp() {
        ExpressionMapper expressionMapper = new DefaultExpressionMapper();
        service = new ProcessorServiceImpl(expressionMapper);
    }

    @Test
    void test_01_create_nfa_from_file() {
        // GIVEN
        String filename = "src\\test\\resources\\data1.txt";

        // WHEN
        ProcessorResult<Expression> result = service.readAutomatonFromFile(filename);

        // THEN
        assertNotNull(result);
        assertThat(result).hasSize(1)
                          .hasValue(0, "((a(a*)b)|(a(b*)b))");
    }

}
