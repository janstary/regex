package com.myself.application.service;

import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;

public interface ConverterService {

    /**
     * Converts NFA to corresponding DFA.
     *
     * @param nfa FiniteAutomaton to be converted
     * @return Created DFA
     */
    DFA<State, String> convertNFAToDFA(NFA<State, String> nfa);

}
