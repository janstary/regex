package com.myself.application.mapping.impl;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.regex.creator.model.Expression;

import java.util.stream.Stream;

import static com.myself.application.regex.creator.model.ExpressionType.STATE;
import static com.myself.application.regex.creator.model.ExpressionType.TRANSITION;
import static java.lang.String.join;

public class DefaultExpressionMapper implements ExpressionMapper {

    @Override
    public Expression asExpression(com.myself.application.processor.model.Expression input) {
        if (input.getValue().matches("(.*),(.*|\\s),(.*)")) { // maybe refactor
            return new Expression(TRANSITION, Stream.of(input.getValue().split(","))
                                                    .map(String::trim)
                                                    .toArray(String[]::new));
        } else if (input.getValue().matches("(.*)(I\\s*|F\\s*|IF\\s*)")) {
            return new Expression(STATE, Stream.of(input.getValue().split("\\s"))
                                               .filter(str -> !str.isEmpty())
                                               .toArray(String[]::new));
        } else if (input.getValue().matches("(\\s*\\D)(\\d+\\s*)")) {
            return new Expression(STATE, Stream.of(input.getValue().split("\\s"))
                                               .filter(str -> !str.isEmpty())
                                               .toArray(String[]::new));
        }
        return null; // TODO should throw exception?
    }

    @Override
    public com.myself.application.processor.model.Expression asExpression(State state, String symbol) {
        String value = join(" ", state.getName(), symbol);
        return new com.myself.application.processor.model.Expression(value);
    }

    @Override
    public com.myself.application.processor.model.Expression asExpression(Transition<State, String> transition) {
        String value = join(",",
                            transition.getFrom().getName(),
                            transition.getSymbol(),
                            transition.getTo().getName());
        return new com.myself.application.processor.model.Expression(value);
    }

}
