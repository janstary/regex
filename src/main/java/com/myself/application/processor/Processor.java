package com.myself.application.processor;

import com.myself.application.processor.model.ProcessorResult;

import java.util.Collection;

/**
 * @param <Q> Type of entity to be processed
 */
public interface Processor<Q> {

    /**
     * Reads source and transform it to Collections of specific type.
     *
     * @return Collection of specific type
     */
    ProcessorResult<Q> read();

    /**
     * @param entity Collections of specific type to be processed further
     * @return Another interface to further process data
     */
    Processor1 data(Collection<Q> entity);

}
