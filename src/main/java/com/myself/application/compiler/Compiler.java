package com.myself.application.compiler;

import com.myself.application.compiler.model.Description;

import java.util.List;

/**
 * Interface represents compiler
 */
public interface Compiler {

    /**
     * Compiles an expression (string value)
     *
     * @param expression string value to be compiled
     */
    List<Description> compile(String expression);

}
