package com.myself.application.converter;

/**
 * Interface represents converter between two types.
 *
 * @param <T> type of entity to be created
 */
public interface Converter<T> {

    /**
     * Converts one entity to another one.
     *
     * @return Converted entity
     */
    T convert();

}
