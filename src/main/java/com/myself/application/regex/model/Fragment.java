package com.myself.application.regex.model;

/**
 * Represents different fragments of regular expression, e.g.
 * the UNION fragment defines the "OR" operation. It is also known as "disjunction" operation.
 */
public enum Fragment {
    CHARACTER,
    EPSILON,
    CONCATENATION,
    UNION,
    ITERATION
}
