package com.myself.application.service.impl;

import com.myself.application.converter.impl.NFAtoDFAConverter;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;
import com.myself.application.service.ConverterService;

public class ConverterServiceImpl implements ConverterService {

    @Override
    public DFA<State, String> convertNFAToDFA(NFA<State, String> nfa) {
        return NFAtoDFAConverter.builder()
                                .nfa(nfa)
                                .build()
                                .convert();
    }

}
