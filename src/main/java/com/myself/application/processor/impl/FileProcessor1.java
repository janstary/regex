package com.myself.application.processor.impl;

import com.myself.application.processor.CouldNotProcessFileException;
import com.myself.application.processor.Processor1;
import com.myself.application.processor.model.Expression;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static java.lang.String.format;
import static java.nio.file.Paths.get;
import static java.util.Objects.requireNonNull;

public class FileProcessor1 implements Processor1 {

    /**
     * Represents the filename
     */
    private final String filename;
    /**
     * List of expressions to be written in file
     */
    private final List<Expression> expressions;

    FileProcessor1(String filename, List<Expression> expressions) {
        this.filename = requireNonNull(filename);
        this.expressions = requireNonNull(expressions);
    }

    /**
     * Writes expressions to file specified by filename.
     */
    public void write() {

        try {
            Files.write(get(this.filename),
                        (Iterable<String>) this.expressions.stream()
                                                           .map(Expression::getValue)
                                                           .map(Object::toString)::iterator);
        } catch (IOException e) {
            throw new CouldNotProcessFileException(
                    format("Could not write to file %s", this.filename), e);
        }
    }
}
