package com.myself.application.regex;

import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.regex.impl.DefaultRegexCreator;
import com.myself.application.regex.model.Definition;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.common.assertions.FiniteAutomataAssert.assertThat;
import static com.myself.application.regex.model.Fragment.CHARACTER;
import static com.myself.application.regex.model.Fragment.CONCATENATION;
import static com.myself.application.regex.model.Fragment.ITERATION;
import static com.myself.application.regex.model.Fragment.UNION;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultRegexCreatorTest {

    @Test
    void test_01() {
        // GIVEN
        Definition definition1 = new Definition(CHARACTER, new int[] {}, "a");
        Definition definition2 = new Definition(CHARACTER, new int[] {}, "b");
        Definition definition3 = new Definition(CONCATENATION, new int[] {1, 2}, "");

        // WHEN
        FiniteAutomaton result = DefaultRegexCreator.builder()
                                                    .definitions(asList(definition1, definition2, definition3))
                                                    .build()
                                                    .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(1)
                          .hasStatesSize(4)
                          .hasTransitionsSize(3)
                          .hasStartState("q0");
    }

    @Test
    void test_02() {
        // GIVEN
        Definition definition1 = new Definition(CHARACTER, new int[] {}, "a");
        Definition definition2 = new Definition(CHARACTER, new int[] {}, "b");
        Definition definition3 = new Definition(UNION, new int[] {1, 2}, "");

        // WHEN
        FiniteAutomaton result = DefaultRegexCreator.builder()
                                                    .definitions(asList(definition1, definition2, definition3))
                                                    .build()
                                                    .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(1)
                          .hasStatesSize(6)
                          .hasTransitionsSize(6)
                          .hasStartState("q0");
    }

    @Test
    void test_03() {
        // GIVEN
        Definition definition1 = new Definition(CHARACTER, new int[] {}, "a");
        Definition definition2 = new Definition(CHARACTER, new int[] {}, "b");
        Definition definition3 = new Definition(ITERATION, new int[] {1}, "");

        // WHEN
        FiniteAutomaton result = DefaultRegexCreator.builder()
                                                    .definitions(asList(definition1, definition2, definition3))
                                                    .build()
                                                    .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(1)
                          .hasStatesSize(4)
                          .hasTransitionsSize(5)
                          .hasStartState("q0");
    }

}
