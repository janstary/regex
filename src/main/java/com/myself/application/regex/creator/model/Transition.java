package com.myself.application.regex.creator.model;

import java.util.Objects;

/**
 * Class represents transition between two specific states.
 */
public class Transition {

    /**
     * Exactly defines the symbol to activate the transition
     */
    private final String symbol;
    /**
     * Represents the initial state of transition
     */
    private final State from;
    /**
     * Represents the final state of transition
     */
    private final State to;

    public Transition(String symbol, State from, State to) {
        this.symbol = symbol;
        this.from = from;
        this.to = to;
    }

    public String getSymbol() {
        return symbol;
    }

    public State getFrom() {
        return from;
    }

    public State getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transition that = (Transition) o;
        return Objects.equals(symbol, that.symbol) &&
               Objects.equals(from, that.from) &&
               Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, from, to);
    }

}
