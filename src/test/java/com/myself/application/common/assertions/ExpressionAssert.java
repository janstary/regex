package com.myself.application.common.assertions;

import com.myself.application.regex.creator.model.Expression;
import com.myself.application.regex.creator.model.ExpressionType;
import org.assertj.core.api.AbstractAssert;

import java.util.Arrays;
import java.util.Objects;

public class ExpressionAssert extends AbstractAssert<ExpressionAssert, Expression> {

    private ExpressionAssert(Expression actual) {
        super(actual, ExpressionAssert.class);
    }

    public static ExpressionAssert assertThat(Expression actual) {
        return new ExpressionAssert(actual);
    }

    public ExpressionAssert hasType(ExpressionType type) {
        isNotNull();

        if (!Objects.equals(actual.getType(), type)) {
            failWithMessage("Expected expression to have type <%s> but had <%s>.",
                            type,
                            actual.getType());
        }
        return this;
    }

    public ExpressionAssert hasValue(String[] value) {
        isNotNull();

        if (!Arrays.equals(actual.getValue(), value)) {
            failWithMessage("Expected expression to have value <%s> but had <%s>.",
                            value,
                            actual.getValue());
        }
        return this;
    }

}
