package com.myself.application.mapping.impl;

import com.myself.application.converter.model.State;
import com.myself.application.mapping.StateMapper;

public class DefaultStateMapper implements StateMapper {

    @Override
    public State asState(com.myself.application.regex.creator.model.State input) {
        return State.builder()
                    .name(input.getName())
                    .build();
    }
}
