package com.myself.application.compiler.impl;

import com.myself.application.compiler.Parser;
import com.myself.application.compiler.exception.NotSupportedOperationException;
import com.myself.application.compiler.model.AST;
import com.myself.application.compiler.model.OperandNode;
import com.myself.application.compiler.model.Token;

import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class represents parser of regex grammar.
 */
public class RegexParser implements Parser {

    /**
     *
     */
    private final AtomicInteger counter = new AtomicInteger(1);
    private final Stack<Token> operatorStack = new Stack<>();
    private final Stack<OperandNode> operandStack = new Stack<>();

    /**
     * Method implements Shunting-yard algorithm.
     *
     * @param tokens sequence of tokens produced by lexer
     * @return Node representing Abstract Syntax Tree
     */
    @Override
    public AST<OperandNode> parse(List<Token> tokens) {
        for (Token token : tokens) {
            switch (token.getType()) {
                case LEFT_BRACKET:
                    operatorStack.push(token);
                    break;
                case CHARACTER_AUTOMATON:
                case EPSILON_AUTOMATON:
                    operandStack.push(createOperandNode(token));
                    break;
                case RIGHT_BRACKET:
                    while (!operatorStack.peek().getSymbol().equals("(")) {
                        Token tkn = operatorStack.pop();

                        operandStack.push(createOperandNode(tkn));
                    }

                    operatorStack.pop();
                    break;
                case CONCATENATION:
                case ITERATION:
                case UNION:
                    while (!operatorStack.empty() &&
                           operatorStack.peek().getType().getPrecedence() >= token.getType().getPrecedence()) {
                        Token tkn = operatorStack.pop();

                        operandStack.push(createOperandNode(tkn));
                    }
                    operatorStack.push(token);
            }
        }

        while (!operatorStack.empty()) {
            Token token = operatorStack.pop();

            operandStack.push(createOperandNode(token));
        }

        return new AST<>(operandStack.pop());
    }

    /**
     * Creates OperandNode according to token and stack.
     *
     * @param token Token to be processed
     * @return New OperandNode
     * @throws NotSupportedOperationException when the operation is nor supported
     */
    private OperandNode createOperandNode(Token token) throws NotSupportedOperationException {
        switch (token.getType().getNumOfOperands()) {
            case 2:
                OperandNode right = operandStack.pop();
                OperandNode left = operandStack.pop();

                return OperandNode.builder()
                                  .index(counter.getAndIncrement())
                                  .value(token.getSymbol())
                                  .operation(token.getType())
                                  .left(left)
                                  .right(right)
                                  .build();
            case 1:
                OperandNode node = operandStack.pop();

                return OperandNode.builder()
                                  .index(counter.getAndIncrement())
                                  .value(token.getSymbol())
                                  .operation(token.getType())
                                  .left(node)
                                  .build();
            case 0:
                return OperandNode.builder()
                                  .index(counter.getAndIncrement())
                                  .value(token.getSymbol())
                                  .operation(token.getType())
                                  .build();
        }

        throw new NotSupportedOperationException(
                "Operation with different operand number than [0,1,2] is not supported.");
    }
}
