package com.myself.application.service;

import com.myself.application.compiler.model.Description;
import com.myself.application.regex.creator.model.FiniteAutomaton;

import java.util.List;

/**
 * Interface represents set of methods to provide business logic of the application.
 */
public interface RegexService {

    /**
     * Process file and creates FiniteAutomaton.
     *
     * @param descriptions file to process
     * @return Created FiniteAutomaton
     */
    FiniteAutomaton createFA(List<Description> descriptions);

}
