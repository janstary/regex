package com.myself.application.common.assertions;

import com.myself.application.compiler.model.Token;
import com.myself.application.compiler.model.TokenType;
import org.assertj.core.api.AbstractAssert;

import java.util.List;
import java.util.Objects;

public class TokenListAssert extends AbstractAssert<TokenListAssert, List<Token>> {

    private TokenListAssert(List<Token> actual) {
        super(actual, TokenListAssert.class);
    }

    public static TokenListAssert assertThat(List<Token> actual) {
        return new TokenListAssert(actual);
    }

    public TokenListAssert hasTypeAndSymbol(int index, TokenType type, String symbol) {
        isNotNull();

        if (!Objects.equals(actual.get(index).getType(), type)) {
            failWithMessage("Expected token at index <%s> to have type <%s> but had <%s>.",
                            index,
                            type,
                            actual.get(index).getType());
        }

        if (!Objects.equals(actual.get(index).getSymbol(), symbol)) {
            failWithMessage("Expected token at index <%s> to have symbol <%s> but had <%s>.",
                            index,
                            symbol,
                            actual.get(index).getSymbol());
        }
        return this;
    }

}
