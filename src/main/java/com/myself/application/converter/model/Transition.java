package com.myself.application.converter.model;

import java.util.Objects;

/**
 * Class represents transition between two specific states.
 *
 * @param <Q> state type
 * @param <E> alphabet type
 */
public class Transition<Q, E> {

    /**
     * Exactly defines the symbol to activate the transition
     */
    private final E symbol;
    /**
     * Represents the initial state of transition
     */
    private final Q from;
    /**
     * Represents the final state of transition
     */
    private final Q to;

    public Transition(E symbol, Q from, Q to) {
        this.symbol = symbol;
        this.from = from;
        this.to = to;
    }

    public static TransitionBuilder builder() { return new TransitionBuilder(); }

    public E getSymbol() {
        return symbol;
    }

    public Q getFrom() {
        return from;
    }

    public Q getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transition<?, ?> that = (Transition<?, ?>) o;
        return Objects.equals(symbol, that.symbol) &&
               Objects.equals(from, that.from) &&
               Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, from, to);
    }

    /**
     * Builder of the class
     *
     * @param <Q>
     * @param <E>
     */
    public static final class TransitionBuilder<Q, E> {

        private E symbol;
        private Q from;
        private Q to;

        private TransitionBuilder() {}

        public TransitionBuilder symbol(E symbol) {
            this.symbol = symbol;
            return this;
        }

        public TransitionBuilder from(Q from) {
            this.from = from;
            return this;
        }

        public TransitionBuilder to(Q to) {
            this.to = to;
            return this;
        }

        public Transition build() { return new Transition<>(symbol, from, to); }
    }
}
