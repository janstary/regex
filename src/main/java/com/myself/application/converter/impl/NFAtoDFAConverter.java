package com.myself.application.converter.impl;

import com.myself.application.converter.Converter;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;

/**
 * Class represent converter between NFA and DFA.
 */
public class NFAtoDFAConverter implements Converter<DFA> {

    /**
     * NFA to be converted
     */
    private final NFA<State, String> nfa;
    /**
     * Represents closure for every state of the finite automaton
     */
    private final Map<State, Set<State>> closures = new HashMap<>();
    /**
     * Represents all states (newly created) of DFA
     */
    private final Map<String, State> states = new HashMap<>();
    /**
     * Newly created transitions of the DFA
     */
    private final Set<Transition<State, String>> transitions = new HashSet<>();

    private NFAtoDFAConverter(NFA<State, String> nfa) {
        this.nfa = requireNonNull(nfa, "NFA should be provided.");
    }

    public static NFAtoDFAConverterBuilder builder() {
        return new NFAtoDFAConverterBuilder();
    }

    /**
     * Converts NFA to DFA.
     * <p>
     * Algorithm:
     * <b>Step 1</b> : Take ∈ closure for the beginning state of NFA as beginning state of DFA.
     * <b>Step 2</b> : Find the states that can be traversed from the present for each input symbol
     * (union of transition value and their closures for each states of NFA present in current state of DFA).
     * <p>
     * <b>Step 3</b> : If any new state is found take it as current state and repeat step 2.
     * <b>Step 4</b> : Do repeat Step 2 and Step 3 until no new state present in DFA transition table.
     * <b>Step 5</b> : Mark the states of DFA which contains final state of NFA as final states of DFA.
     *
     * @return Created DFA
     */
    @Override
    public DFA<State, String> convert() {
        for (State state : this.nfa.getStates()) {
            closure(state);
        }

        Set<State> startState = this.closures.get(this.nfa.getStartState());

        computeTransitions(startState);

        return new DFA<>(this.states.get(concatNames(startState)),
                         getAllAcceptStates(),
                         this.transitions,
                         new HashSet<>(this.states.values()));
    }

    /**
     * Calculates set of states which can be reached from the startState with only (null)
     * or ε moves including the startState itself (closure). In other words, ε-closure for a state
     * can be obtained by union operation of the ε-closure of the states which can be reached from
     * startState with a single ε move in recursive manner.
     *
     * @param startState start state
     */
    private void closure(State startState) {

        if (this.closures.containsKey(startState)) {
            return;
        }

        List<State> states = this.nfa.transitionFunction(startState, "");

        this.closures.computeIfAbsent(startState, s -> new HashSet<>());
        this.closures.get(startState).add(startState);

        for (State state : states) {
            closure(state);
            this.closures.get(startState).addAll(this.closures.get(state));
        }
    }

    /**
     * Computes all possible states of DFA corresponding to NFA, in recursive manner.
     *
     * @param states Set of states of NDA representing exactly one state in DFA
     */
    private void computeTransitions(Set<State> states) {

        String stateName = concatNames(states);

        if (this.states.containsKey(stateName)) {
            return;
        }

        this.states.put(stateName, new State(stateName));

        Map<String, Set<State>> allPossibleStates = getAllPossibleStates(states);

        for (Entry<String, Set<State>> entry : allPossibleStates.entrySet()) {
            Set<State> entryStates = entry.getValue();
            entryStates.addAll(getClosures(entry.getValue()));

            computeTransitions(entryStates);

            Transition<State, String> transition = new Transition<>(entry.getKey(),
                                                                    this.states.get(stateName),
                                                                    this.states.get(concatNames(entry.getValue())));
            this.transitions.add(transition);
        }

    }

    /**
     * @param states Set of states
     * @return Map of all possible/reachable states
     */
    private Map<String, Set<State>> getAllPossibleStates(Set<State> states) {
        return this.nfa.getTransitions()
                       .stream()
                       .filter(transition -> states.contains(transition.getFrom()))
                       .filter(transition -> !transition.getSymbol().isEmpty())
                       .collect(groupingBy(Transition::getSymbol,
                                           mapping(Transition::getTo,
                                                   toSet())));
    }

    /**
     * Calculates the closure for corresponding states.
     *
     * @param states Set of states
     * @return set of states
     */
    private Set<State> getClosures(Set<State> states) {
        return this.closures.entrySet()
                            .stream()
                            .filter(entry -> states.contains(entry.getKey()))
                            .map(Entry::getValue)
                            .flatMap(Set::stream)
                            .collect(toSet());
    }

    /**
     * Concatenate names of input states.
     *
     * @param states Collections of states to be connected
     * @return name of connected states
     */
    private String concatNames(Collection<State> states) {
        return states.stream()
                     .map(State::getName)
                     .sorted()
                     .collect(joining());
    }

    /**
     * Computes all accept states of DFA.
     *
     * @return Set of all accept states
     */
    private Set<State> getAllAcceptStates() {
        return this.states.values()
                          .stream()
                          .filter(this::isAcceptState)
                          .collect(toSet());
    }

    /**
     * Finds out if state is accept by DFA or not.
     *
     * @param state state to be processed
     * @return True if state is accept or false if is not
     */
    private boolean isAcceptState(State state) {
        return this.nfa.getAcceptStates()
                       .stream()
                       .anyMatch(s -> state.getName().contains(s.getName()));
    }

    /**
     * Builder of the class.
     */
    public static final class NFAtoDFAConverterBuilder {

        private NFA<State, String> nfa;

        private NFAtoDFAConverterBuilder() {}

        public NFAtoDFAConverterBuilder nfa(NFA<State, String> nfa) {
            this.nfa = nfa;
            return this;
        }

        public NFAtoDFAConverter build() { return new NFAtoDFAConverter(nfa); }
    }
}
