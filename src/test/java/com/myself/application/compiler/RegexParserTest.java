package com.myself.application.compiler;

import com.myself.application.compiler.impl.RegexParser;
import com.myself.application.compiler.model.AST;
import com.myself.application.compiler.model.OperandNode;
import com.myself.application.compiler.model.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static com.myself.application.compiler.model.TokenType.*;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class RegexParserTest {

    private Parser parser;

    @BeforeEach
    void setUp() {
        this.parser = new RegexParser();
    }

    @Test
    void test_01_success() {
        // GIVEN
        List<Token> tokens = asList(new Token(LEFT_BRACKET, "("),
                                    new Token(CHARACTER_AUTOMATON, "a"),
                                    new Token(UNION, "|"),
                                    new Token(CHARACTER_AUTOMATON, "b"),
                                    new Token(RIGHT_BRACKET, ")"),
                                    new Token(CONCATENATION, ""),
                                    new Token(CHARACTER_AUTOMATON, "a"));

        // WHEN
        AST<OperandNode> result = parser.parse(tokens);

        // THEN
        assertNotNull(result);
    }

    @Test
    void test_02_success() {
        // GIVEN
        List<Token> tokens = asList(new Token(LEFT_BRACKET, "("),
                                    new Token(CHARACTER_AUTOMATON, "a"),
                                    new Token(UNION, "|"),
                                    new Token(CHARACTER_AUTOMATON, "b"),
                                    new Token(RIGHT_BRACKET, ")"),
                                    new Token(ITERATION, "*"));

        // WHEN
        AST<OperandNode> result = parser.parse(tokens);

        // THEN
        assertNotNull(result);
    }

}
