package com.myself.application.mapping;

import com.myself.application.common.assertions.DFAAssert;
import com.myself.application.common.assertions.NFAAssert;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.mapping.impl.DefaultAutomatonMapper;
import com.myself.application.mapping.impl.DefaultStateMapper;
import com.myself.application.mapping.impl.DefaultTransitionMapper;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.regex.creator.model.State;
import com.myself.application.regex.creator.model.Transition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static java.util.Set.of;

@TestMethodOrder(Alphanumeric.class)
public class DefaultAutomatonMapperTest {

    private AutomatonMapper<com.myself.application.converter.model.State, String> automatonMapper;

    @BeforeEach
    void setUp() {
        StateMapper stateMapper = new DefaultStateMapper();
        TransitionMapper transitionMapper = new DefaultTransitionMapper(stateMapper);
        this.automatonMapper = new DefaultAutomatonMapper(stateMapper, transitionMapper);
    }

    @Test
    void test_01_as_nfa_success() {
        // GIVEN
        FiniteAutomaton finiteAutomaton = createFiniteAutomata();

        // WHEN
        NFA<com.myself.application.converter.model.State, String> result = automatonMapper.asNFA(finiteAutomaton);

        // THEN
        NFAAssert.assertThat(result)
                 .hasAcceptState("q0")
                 .hasAcceptState("q1")
                 .hasStartState("q0")
                 .hasState("q2")
                 .hasTransition("q0", "a", "q1")
                 .hasTransition("q0", "b", "q2");
    }

    @Test
    void test_02_as_dfa_success() {
        // GIVEN
        FiniteAutomaton finiteAutomaton = createFiniteAutomata();

        // WHEN
        DFA<com.myself.application.converter.model.State, String> result = automatonMapper.asDFA(finiteAutomaton);

        // THEN
        DFAAssert.assertThat(result)
                 .hasAcceptState("q0")
                 .hasAcceptState("q1")
                 .hasStartState("q0")
                 .hasState("q2")
                 .hasTransition("q0", "a", "q1")
                 .hasTransition("q0", "b", "q2");
    }

    private FiniteAutomaton createFiniteAutomata() {
        State state1 = new State("q0", true, true);
        State state2 = new State("q1", true, false);
        State state3 = new State("q2", false, false);

        Transition transition1 = new Transition("a", state1, state2);
        Transition transition2 = new Transition("b", state1, state3);

        return new FiniteAutomaton(state1,
                                   of(state1, state2),
                                   of(transition1, transition2),
                                   of(state1, state2, state3));
    }

}
