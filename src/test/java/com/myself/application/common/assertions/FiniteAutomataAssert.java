package com.myself.application.common.assertions;

import com.myself.application.regex.creator.model.FiniteAutomaton;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class FiniteAutomataAssert extends AbstractAssert<FiniteAutomataAssert, FiniteAutomaton> {

    private FiniteAutomataAssert(FiniteAutomaton actual) {
        super(actual, FiniteAutomataAssert.class);
    }

    public static FiniteAutomataAssert assertThat(FiniteAutomaton actual) {
        return new FiniteAutomataAssert(actual);
    }

    public FiniteAutomataAssert hasStartState(String stateName) {
        isNotNull();

        if (!Objects.equals(actual.getStartState().getName(), stateName)) {
            failWithMessage("Expected finite automata to have start state <%s> but had <%s>.",
                            stateName,
                            actual.getStartState().getName());
        }
        return this;
    }

    public FiniteAutomataAssert hasStatesSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getStates().size(), size)) {
            failWithMessage("Expected finite automata to have states size <%s> but had <%s>.",
                            size,
                            actual.getStates().size());
        }
        return this;
    }

    public FiniteAutomataAssert hasAcceptStatesSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getAcceptStates().size(), size)) {
            failWithMessage("Expected finite automata to have accept states size <%s> but had <%s>.",
                            size,
                            actual.getAcceptStates().size());
        }
        return this;
    }

    public FiniteAutomataAssert hasTransitionsSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getTransitions().size(), size)) {
            failWithMessage("Expected finite automata to have transitions size <%s> but had <%s>.",
                            size,
                            actual.getTransitions().size());
        }
        return this;
    }

}
