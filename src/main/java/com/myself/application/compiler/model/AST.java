package com.myself.application.compiler.model;

import java.util.Objects;

/**
 * Class represents Abstract Syntax Tree
 *
 * @param <T> specifies type of the node
 */
public class AST<T> {

    /**
     * Root node of the Abstract Syntax Tree
     */
    private final T root;

    public AST(T root) {
        this.root = root;
    }

    public T getRoot() {
        return root;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AST<?> ast = (AST<?>) o;
        return Objects.equals(root, ast.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root);
    }
}
