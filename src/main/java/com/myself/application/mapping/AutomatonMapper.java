package com.myself.application.mapping;

import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.regex.creator.model.FiniteAutomaton;

/**
 * Interface maps FiniteAutomaton from creator package to specific implementation
 * of FiniteAutomaton from converter package.
 *
 * @param <Q> state type
 * @param <E> alphabet type
 */
public interface AutomatonMapper<Q, E> {

    /**
     * Converts finite automata from creator package to specific
     * implementation of NFA from converter package.
     *
     * @param input Finite automata to be converted
     * @return Mapped NFA
     */
    NFA<Q, E> asNFA(FiniteAutomaton input);

    /**
     * Converts finite automata from creator package to specific
     * implementation of DFA from converter package.
     *
     * @param input Finite automata to be converted
     * @return Mapped DFA
     */
    DFA<Q, E> asDFA(FiniteAutomaton input);
}
