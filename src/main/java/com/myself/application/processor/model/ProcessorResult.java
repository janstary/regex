package com.myself.application.processor.model;

import java.util.List;
import java.util.Objects;

/**
 * Class represents unified unit of the processing result.
 *
 * @param <Q>
 */
public class ProcessorResult<Q> {

    /**
     * Represents result of processing
     */
    private final List<Q> data;

    public ProcessorResult(List<Q> data) {
        this.data = data;
    }

    public List<Q> getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcessorResult that = (ProcessorResult) o;
        return Objects.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }
}
