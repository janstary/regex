package com.myself.application.compiler;

import com.myself.application.compiler.exception.NoSuchElementException;
import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.OperandNode;

public interface DescriptionFactory {

    /**
     * @param node node to be converted
     * @return new Description derived from node
     * @throws NoSuchElementException if none of Description matches the input node
     */
    Description createDescription(OperandNode node) throws NoSuchElementException;

}
