package com.myself.application.regex.model;

import java.util.Arrays;
import java.util.Objects;

/**
 * Represents definition of regular expression, in other words,
 * specific regular expression operation with data relative to the operation.
 */
public class Definition {

    /**
     * Specify the type of operation/fragment of the regular expression
     */
    private final Fragment fragment;
    /**
     * Indexes of automatons to be included into operation, e.g. UNION
     */
    private final int[] indexes;
    /**
     * Symbol which automaton should accept, e.g. 'a' stands for automaton that should accepts 'a'
     */
    private final String symbol;

    public Definition(Fragment fragment, int[] indexes, String symbol) {
        this.fragment = fragment;
        this.indexes = indexes;
        this.symbol = symbol;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public int[] getIndexes() {
        return indexes;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Definition that = (Definition) o;
        return fragment == that.fragment &&
               Arrays.equals(indexes, that.indexes) &&
               Objects.equals(symbol, that.symbol);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(fragment, symbol);
        result = 31 * result + Arrays.hashCode(indexes);
        return result;
    }
}
