package com.myself.application.common.assertions;

import com.myself.application.compiler.model.Token;
import com.myself.application.compiler.model.TokenType;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class TokenAssert extends AbstractAssert<TokenAssert, Token> {

    private TokenAssert(Token actual) {
        super(actual, TokenAssert.class);
    }

    public static TokenAssert assertThat(Token actual) {
        return new TokenAssert(actual);
    }

    public TokenAssert hasType(TokenType type) {
        isNotNull();

        if (!Objects.equals(actual.getType(), type)) {
            failWithMessage("Expected token to have type <%s> but had <%s>.",
                            type,
                            actual.getType());
        }
        return this;
    }

    public TokenAssert hasSymbol(String symbol) {
        isNotNull();

        if (!Objects.equals(actual.getSymbol(), symbol)) {
            failWithMessage("Expected token to have symbol <%s> but had <%s>.",
                            symbol,
                            actual.getSymbol());
        }
        return this;
    }
}
