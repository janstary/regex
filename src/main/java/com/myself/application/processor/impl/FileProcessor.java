package com.myself.application.processor.impl;

import com.myself.application.processor.CouldNotProcessFileException;
import com.myself.application.processor.Processor;
import com.myself.application.processor.Processor1;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Class is responsible for processing files in specific format.
 */
public class FileProcessor implements Processor<Expression> {

    /**
     * Represents the source
     */
    private String source;

    private FileProcessor(String source) {
        this.source = requireNonNull(source, "Filename should be provided.");
    }

    public static FileProcessor of(String filename) {
        return new FileProcessor(filename);
    }

    @Override
    public Processor1 data(Collection<Expression> expressions) {
        return new FileProcessor1(this.source, new ArrayList<>(expressions));
    }

    /**
     * Reads file and transforms each line in Description object.
     *
     * @return List of expressions
     * @throws CouldNotProcessFileException if file could not be processed
     */
    @Override
    public ProcessorResult<Expression> read() throws CouldNotProcessFileException {
        requireNonNull(this.source);

        try (Stream<String> lines = lines(get(this.source))) {
            return new ProcessorResult<>(lines.map(String::trim)
                                              .map(Expression::new)
                                              .collect(toList()));

        } catch (IOException e) {
            throw new CouldNotProcessFileException(
                    format("Could not open a file '%s'", this.source), e);
        }
    }

}
