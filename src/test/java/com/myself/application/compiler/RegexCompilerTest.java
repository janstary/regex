package com.myself.application.compiler;

import com.myself.application.common.assertions.DescriptionListAssert;
import com.myself.application.compiler.impl.DefaultDescriptionFactory;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.impl.RegexCompiler;
import com.myself.application.compiler.impl.RegexLexer;
import com.myself.application.compiler.impl.RegexParser;
import com.myself.application.compiler.model.Description;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static com.myself.application.compiler.model.Fragment.CHARACTER;
import static com.myself.application.compiler.model.Fragment.CONCATENATION;
import static com.myself.application.compiler.model.Fragment.ITERATION;
import static com.myself.application.compiler.model.Fragment.UNION;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class RegexCompilerTest {

    private RegexCompiler compiler;

    @BeforeEach
    void setUp() {
        TokenFactory tokenFactory = new DefaultTokenFactory();
        DescriptionFactory descriptionFactory = new DefaultDescriptionFactory();
        Lexer lexer = new RegexLexer(tokenFactory);
        Parser parser = new RegexParser();
        this.compiler = new RegexCompiler(lexer, parser, descriptionFactory);
    }

    @Test
    void test_01_success() {
        // GIVEN
        String regex = "(a|b)|a";

        // WHEN
        List<Description> result = compiler.compile(regex);

        // THEN
        assertNotNull(result);
        DescriptionListAssert.assertThat(result)
                             .hasValue(0, "a")
                             .hasOperation(0, CHARACTER)
                             .hasValue(1, "b")
                             .hasOperation(1, CHARACTER)
                             .hasValue(2, "|")
                             .hasOperation(2, UNION)
                             .hasIndex(2, 0, 1)
                             .hasIndex(2, 1, 2)
                             .hasValue(3, "a")
                             .hasOperation(3, CHARACTER)
                             .hasValue(4, "|")
                             .hasOperation(4, UNION)
                             .hasIndex(4, 0, 3)
                             .hasIndex(4, 1, 4);

    }

    @Test
    void test_02_success() {
        // GIVEN
        String regex = "(a|b)a";

        // WHEN
        List<Description> result = compiler.compile(regex);

        // THEN
        assertNotNull(result);
        DescriptionListAssert.assertThat(result)
                             .hasValue(0, "a")
                             .hasOperation(0, CHARACTER)
                             .hasValue(1, "b")
                             .hasOperation(1, CHARACTER)
                             .hasValue(2, "|")
                             .hasOperation(2, UNION)
                             .hasIndex(2, 0, 1)
                             .hasIndex(2, 1, 2)
                             .hasValue(3, "a")
                             .hasOperation(3, CHARACTER)
                             .hasValue(4, ".")
                             .hasOperation(4, CONCATENATION)
                             .hasIndex(4, 0, 3)
                             .hasIndex(4, 1, 4);
    }

    @Test
    void test_03_success() {
        // GIVEN
        String regex = "((a(a*)b)|(a(b*)b))";

        // WHEN
        List<Description> result = compiler.compile(regex);

        // THEN
        assertNotNull(result);
        DescriptionListAssert.assertThat(result)
                             .hasValue(0, "a")
                             .hasOperation(0, CHARACTER)
                             .hasValue(1, "a")
                             .hasOperation(1, CHARACTER)
                             .hasValue(2, "*")
                             .hasOperation(2, ITERATION)
                             .hasIndex(2, 0, 2)
                             .hasIndexLength(2, 1)
                             .hasValue(3, ".")
                             .hasOperation(3, CONCATENATION)
                             .hasIndex(3, 0, 1)
                             .hasIndex(3, 1, 3)
                             .hasIndexLength(3, 2)
                             .hasValue(4, "b")
                             .hasOperation(4, CHARACTER)
                             .hasValue(5, ".")
                             .hasOperation(5, CONCATENATION)
                             .hasIndex(5, 0, 4)
                             .hasIndex(5, 1, 5)
                             .hasIndexLength(5, 2)
                             .hasValue(6, "a")
                             .hasOperation(6, CHARACTER)
                             .hasValue(7, "b")
                             .hasOperation(7, CHARACTER)
                             .hasValue(8, "*")
                             .hasOperation(8, ITERATION)
                             .hasValue(9, ".")
                             .hasOperation(9, CONCATENATION)
                             .hasIndex(9, 0, 7)
                             .hasIndex(9, 1, 9)
                             .hasIndexLength(9, 2)
                             .hasValue(10, "b")
                             .hasOperation(10, CHARACTER)
                             .hasValue(11, ".")
                             .hasOperation(11, CONCATENATION)
                             .hasIndex(11, 0, 10)
                             .hasIndex(11, 1, 11)
                             .hasIndexLength(11, 2)
                             .hasValue(12, "|")
                             .hasOperation(12, UNION)
                             .hasIndex(11, 0, 10)
                             .hasIndex(11, 1, 11)
                             .hasIndexLength(11, 2);
    }

}
