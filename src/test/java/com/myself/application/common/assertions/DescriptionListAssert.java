package com.myself.application.common.assertions;

import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.Fragment;
import org.assertj.core.api.AbstractAssert;

import java.util.List;
import java.util.Objects;

public class DescriptionListAssert extends AbstractAssert<DescriptionListAssert, List<Description>> {

    private DescriptionListAssert(List<Description> actual) {
        super(actual, DescriptionListAssert.class);
    }

    public static DescriptionListAssert assertThat(List<Description> actual) {
        return new DescriptionListAssert(actual);
    }

    public DescriptionListAssert hasOperation(int index, Fragment fragment) {
        isNotNull();

        if (!Objects.equals(actual.get(index).getFragment(), fragment)) {
            failWithMessage("Expected description at index [%s] to have fragment <%s> but had <%s>.",
                            index,
                            fragment,
                            actual.get(index).getFragment());
        }
        return this;
    }

    public DescriptionListAssert hasValue(int index, String value) {
        isNotNull();

        if (!Objects.equals(actual.get(index).getValue(), value)) {
            failWithMessage("Expected description at index [%s] to have symbol <%s> but had <%s>.",
                            index,
                            value,
                            actual.get(index).getValue());
        }
        return this;
    }

    public DescriptionListAssert hasIndex(int indexOfDescription, int index, int value) {
        isNotNull();

        if (!Objects.equals(actual.get(indexOfDescription).getIndexes()[index], value)) {
            failWithMessage("Expected description at index [%s] to have index[%s] <%s> but had <%s>.",
                            indexOfDescription,
                            index,
                            value,
                            actual.get(indexOfDescription).getIndexes()[index]);
        }
        return this;
    }

    public DescriptionListAssert hasIndexLength(int index, int length) {
        isNotNull();

        if (!Objects.equals(actual.get(index).getIndexes().length, length)) {
            failWithMessage("Expected description at index [%s] to have index length <%s> but had <%s>.",
                            index,
                            length,
                            actual.get(index).getIndexes().length);
        }
        return this;
    }

}
