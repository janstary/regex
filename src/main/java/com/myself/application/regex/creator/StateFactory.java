package com.myself.application.regex.creator;

import com.myself.application.regex.creator.model.State;

/**
 * Class represents factory for creating States based on input.
 */
public class StateFactory {

    /**
     * Creates instance of state.
     *
     * @param name  name of state
     * @param input input string
     * @return created State
     */
    public static State getState(final String name, final String input) {
        if (input.contains("IF")) { // maybe refactor
            return new State(name, true, true);
        } else if (input.matches("I")) {
            return new State(name, false, true);
        } else if (input.contains("F")) {
            return new State(name, true, false);
        }
        return new State(name, false, false);
    }

}
