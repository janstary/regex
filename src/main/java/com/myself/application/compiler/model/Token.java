package com.myself.application.compiler.model;

import java.util.Objects;

public class Token {

    private final TokenType type;
    private final String symbol;

    public Token(TokenType type, String symbol) {
        this.type = type;
        this.symbol = symbol;
    }

    public TokenType getType() {
        return type;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Token token = (Token) o;
        return type == token.type &&
               Objects.equals(symbol, token.symbol);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, symbol);
    }
}
