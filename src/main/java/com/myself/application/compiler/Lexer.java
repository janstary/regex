package com.myself.application.compiler;

import com.myself.application.compiler.model.Token;

import java.util.List;

/**
 * Interface represents lexer
 */
public interface Lexer {

    /**
     * Creates tokens from expression
     *
     * @param expression input string value
     * @return List of Token
     */
    List<Token> tokenize(String expression);

}
