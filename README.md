#Regex Power-Tool

##Build the program
```bash
mvn clean install
```

##Test the program
```bash
mvn test
```

### Run the program
```bash
cd target\
java -jar .\regex-1.0-SNAPSHOT.jar file
```
