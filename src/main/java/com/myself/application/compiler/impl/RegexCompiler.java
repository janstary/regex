package com.myself.application.compiler.impl;

import com.myself.application.compiler.Compiler;
import com.myself.application.compiler.DescriptionFactory;
import com.myself.application.compiler.Lexer;
import com.myself.application.compiler.Parser;
import com.myself.application.compiler.model.AST;
import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.OperandNode;
import com.myself.application.compiler.model.Token;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Class represents compiler of regex grammar
 */
public class RegexCompiler implements Compiler {

    /**
     * Stores nodes in post-order
     */
    private final List<OperandNode> nodes = new ArrayList<>();

    /**
     * Lexer for used tokenize
     */
    private final Lexer lexer;
    /**
     * Parser used for parsing
     */
    private final Parser parser;
    /**
     * Description factory for creating Description instances
     */
    private final DescriptionFactory descriptionFactory;

    public RegexCompiler(Lexer lexer, Parser parser, DescriptionFactory descriptionFactory) {
        this.lexer = requireNonNull(lexer, "Lexer should be provided.");
        this.parser = requireNonNull(parser, "Parser should be provided.");
        this.descriptionFactory = requireNonNull(descriptionFactory,
                                                 "DescriptionFactory should be provided.");
    }

    @Override
    public List<Description> compile(String expression) {
        List<Token> tokens = lexer.tokenize(expression);

        AST<OperandNode> ast = parser.parse(tokens);

        postOrder(ast.getRoot());

        return nodes.stream()
                    .map(descriptionFactory::createDescription)
                    .collect(toList());
    }

    /**
     * Implements tree traversal in post-order manner
     *
     * @param node root node
     */
    private void postOrder(OperandNode node) {
        if (node == null) {
            return;
        }

        postOrder(node.getLeft());
        postOrder(node.getRight());
        nodes.add(node);
    }
}
