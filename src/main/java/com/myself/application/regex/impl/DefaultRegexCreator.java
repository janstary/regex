package com.myself.application.regex.impl;

import com.myself.application.regex.RegexCreator;
import com.myself.application.regex.SyntaxErrorException;
import com.myself.application.regex.creator.impl.FACreator;
import com.myself.application.regex.creator.model.Expression;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.regex.creator.model.State;
import com.myself.application.regex.creator.model.Transition;
import com.myself.application.regex.model.Definition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import static com.myself.application.regex.creator.model.ExpressionType.STATE;
import static com.myself.application.regex.creator.model.ExpressionType.TRANSITION;
import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

/**
 * Class represents default regular expression creator.
 * Class accepts definitions of regular expression operations used for finite automaton creation.
 */
public class DefaultRegexCreator implements RegexCreator {

    /**
     * Stores created automaton
     */
    private final List<FiniteAutomaton> listOfAutomaton = new ArrayList<>();
    /**
     * Represents expression used for automaton creation
     */
    private final List<Definition> definitions;
    /**
     * Counter is used for state naming convention
     */
    private final AtomicInteger counter = new AtomicInteger(0);

    private DefaultRegexCreator(List<Definition> definitions) {
        this.definitions = requireNonNull(definitions, "Definitions should be provided.");
    }

    public static DefaultRegexCreatorBuilder builder() {
        return new DefaultRegexCreatorBuilder();
    }

    @Override
    public FiniteAutomaton create() throws SyntaxErrorException {
        for (Definition definition : this.definitions) {
            switch (definition.getFragment()) {
                case CHARACTER:
                case EPSILON:
                    listOfAutomaton.add(createCharacterAutomaton(definition.getSymbol()));
                    break;
                case UNION:
                    listOfAutomaton.add(union(listOfAutomaton.get(definition.getIndexes()[0] - 1),
                                              listOfAutomaton.get(definition.getIndexes()[1] - 1)));
                    break;
                case ITERATION:
                    listOfAutomaton.add(iteration(listOfAutomaton.get(definition.getIndexes()[0] - 1)));
                    break;
                case CONCATENATION:
                    listOfAutomaton.add(concatenation(listOfAutomaton.get(definition.getIndexes()[0] - 1),
                                                      listOfAutomaton.get(definition.getIndexes()[1] - 1)));
                    break;
            }
            counter.set(0); // set counter to 0 after every operation
        }

        return listOfAutomaton.get(listOfAutomaton.size() - 1);
    }

    /**
     * Creates new automaton which accepts the specified symbol.
     *
     * @param symbol Symbol that automaton should accept
     * @return Created finite automaton
     */
    private FiniteAutomaton createCharacterAutomaton(String symbol) {
        String state1 = format("q%s", counter.getAndIncrement());
        String state2 = format("q%s", counter.getAndIncrement());

        return FACreator.builder()
                        .expression(new Expression(STATE, new String[] {state1, "I"}))
                        .expression(new Expression(STATE, new String[] {state2, "F"}))
                        .expression(new Expression(TRANSITION, new String[] {state1, symbol, state2}))
                        .build()
                        .create();
    }

    /**
     * The union NFA fragment defines the 'OR' operation.
     * It is also known as 'disjunction' operation.
     *
     * @param automatonA First automaton
     * @param automatonB Second automaton
     * @return Newly created automaton
     * @throws SyntaxErrorException when automaton does not have accept state, inferred from getAcceptStateName
     */
    private FiniteAutomaton union(FiniteAutomaton automatonA, FiniteAutomaton automatonB) throws SyntaxErrorException {
        String state1 = format("q%s", counter.getAndIncrement());
        String state2 = format("q%s", counter.getAndIncrement());

        FiniteAutomaton newAutomatonA = automatonFromAutomaton(automatonA);
        FiniteAutomaton newAutomatonB = automatonFromAutomaton(automatonB);

        return FACreator.builder()
                        .expression(new Expression(STATE, new String[] {state1, "I"}))
                        .expression(new Expression(STATE, new String[] {state2, "F"}))
                        .expression(new Expression(TRANSITION, new String[] {state1,
                                                                             "",
                                                                             newAutomatonA.getStartState().getName()}))
                        .expression(new Expression(TRANSITION, new String[] {state1,
                                                                             "",
                                                                             newAutomatonB.getStartState().getName()}))
                        .expression(new Expression(TRANSITION, new String[] {getAcceptStateName(newAutomatonA),
                                                                             "",
                                                                             state2}))
                        .expression(new Expression(TRANSITION, new String[] {getAcceptStateName(newAutomatonB),
                                                                             "",
                                                                             state2}))
                        .expressions(transformStatesToExpression(newAutomatonA))
                        .expressions(transformStatesToExpression(newAutomatonB))
                        .expressions(transformTransitionToExpression(newAutomatonA))
                        .expressions(transformTransitionToExpression(newAutomatonB))
                        .build()
                        .create();
    }

    /**
     * The iteration operator defines the repetition pattern, and in a more
     * user-friendly way of saying is repeating of a machine 'zero or more times'.
     *
     * @param automaton First automaton
     * @return Newly created automaton
     * @throws SyntaxErrorException when automaton does not have accept state, inferred from getAcceptStateName
     */
    private FiniteAutomaton iteration(FiniteAutomaton automaton) throws SyntaxErrorException {
        String state1 = format("q%s", counter.getAndIncrement());
        String state2 = format("q%s", counter.getAndIncrement());

        FiniteAutomaton newAutomaton = automatonFromAutomaton(automaton);

        return FACreator.builder()
                        .expression(new Expression(STATE, new String[] {state1, "I"}))
                        .expression(new Expression(STATE, new String[] {state2, "F"}))
                        .expression(new Expression(TRANSITION, new String[] {state1,
                                                                             "",
                                                                             newAutomaton.getStartState().getName()}))
                        .expression(new Expression(TRANSITION, new String[] {state1, "", state2}))
                        .expression(new Expression(TRANSITION, new String[] {state2,
                                                                             "",
                                                                             newAutomaton.getStartState().getName()}))
                        .expression(new Expression(TRANSITION, new String[] {getAcceptStateName(newAutomaton),
                                                                             "",
                                                                             state2}))
                        .expressions(transformStatesToExpression(newAutomaton))
                        .expressions(transformTransitionToExpression(newAutomaton))
                        .build()
                        .create();
    }

    /**
     * The concatenation NFA-fragment defines the 'A followed by B' operation.
     *
     * @param automatonA First automaton
     * @param automatonB Second automaton
     * @return Newly created automaton
     * @throws SyntaxErrorException when automaton does not have accept state, inferred from getAcceptStateName
     */
    private FiniteAutomaton concatenation(FiniteAutomaton automatonA, FiniteAutomaton automatonB) throws SyntaxErrorException {
        FiniteAutomaton newAutomatonA = automatonFromAutomaton(automatonA);
        FiniteAutomaton newAutomatonB = automatonFromAutomaton(automatonB);

        return FACreator.builder()
                        .expression(new Expression(STATE, new String[] {newAutomatonA.getStartState().getName(), "I"}))
                        .expression(new Expression(STATE, new String[] {getAcceptStateName(newAutomatonB), "F"}))
                        .expression(new Expression(TRANSITION, new String[] {getAcceptStateName(newAutomatonA),
                                                                             "",
                                                                             newAutomatonB.getStartState().getName()}))
                        .expressions(transformStatesToExpression(newAutomatonA, state -> !state.isInitial()))
                        .expressions(transformStatesToExpression(newAutomatonB, state -> !state.isFinal()))
                        .expressions(transformTransitionToExpression(newAutomatonA))
                        .expressions(transformTransitionToExpression(newAutomatonB))
                        .build()
                        .create();
    }

    /**
     * Recreates automaton with different state names and corresponding transitions.
     *
     * @param automaton automaton to be recreated
     * @return recreated automaton with new states names
     */
    private FiniteAutomaton automatonFromAutomaton(FiniteAutomaton automaton) {
        Map<State, State> stateToState = automaton.getStates()
                                                  .stream()
                                                  .sorted(comparing(State::getName))
                                                  .collect(LinkedHashMap::new,
                                                           (map, state) -> map.put(state,
                                                                                   new State(format("q%s", counter.getAndIncrement()),
                                                                                             state.isFinal(),
                                                                                             state.isInitial())),
                                                           Map::putAll);

        Set<Transition> transitions = automaton.getTransitions()
                                               .stream()
                                               .map(transition -> new Transition(transition.getSymbol(),
                                                                                 stateToState.get(transition.getFrom()),
                                                                                 stateToState.get(transition.getTo())))
                                               .collect(toSet());

        Set<State> acceptStates = stateToState.values()
                                              .stream()
                                              .filter(State::isFinal)
                                              .collect(toSet());

        return new FiniteAutomaton(stateToState.get(automaton.getStartState()),
                                   acceptStates,
                                   transitions,
                                   new HashSet<>(stateToState.values()));
    }

    /**
     * Transforms automaton states filtered by specific predicate to expressions.
     *
     * @param automaton automaton to be processed
     * @param predicate predicate used to filter states
     * @return Set of expression made of automaton states
     */
    private Set<Expression> transformStatesToExpression(FiniteAutomaton automaton,
                                                        Predicate<State> predicate) {
        return automaton.getStates()
                        .stream()
                        .filter(predicate)
                        .map(state -> new Expression(STATE, new String[] {state.getName()}))
                        .collect(toSet());
    }

    /**
     * Transforms all automaton states to expressions.
     *
     * @param automaton Automaton to be processed
     * @return Set of expression made of automaton states
     */
    private Set<Expression> transformStatesToExpression(FiniteAutomaton automaton) {
        return automaton.getStates()
                        .stream()
                        .map(state -> new Expression(STATE, new String[] {state.getName()}))
                        .collect(toSet());
    }

    /**
     * Transforms automaton transitions to expressions.
     *
     * @param automaton Automaton to be processed
     * @return Set of expression made of automaton transitions
     */
    private Set<Expression> transformTransitionToExpression(FiniteAutomaton automaton) {
        return automaton.getTransitions()
                        .stream()
                        .map(transition -> new Expression(TRANSITION,
                                                          new String[] {transition.getFrom().getName(),
                                                                        transition.getSymbol(),
                                                                        transition.getTo().getName()}))
                        .collect(toSet());
    }

    /**
     * Finds first (and only!) accept state of automaton and returns its name.
     *
     * @param automaton Automaton to be processed
     * @return Name of the accept state of automaton
     * @throws SyntaxErrorException when automaton does not have accept state
     */
    private String getAcceptStateName(FiniteAutomaton automaton) throws SyntaxErrorException {
        return automaton.getAcceptStates()
                        .stream()
                        .findFirst()
                        .orElseThrow(
                                () -> new SyntaxErrorException(
                                        "Syntax error! Automaton does not have a accept state")).getName();
    }

    /**
     * Builder of the class
     */
    public static final class DefaultRegexCreatorBuilder {

        private List<Definition> definitions;

        private DefaultRegexCreatorBuilder() {}

        public DefaultRegexCreatorBuilder definition(Definition definition) {
            if (this.definitions == null) {
                this.definitions = new ArrayList<>();
            }

            this.definitions.add(definition);
            return this;
        }

        public DefaultRegexCreatorBuilder definitions(List<Definition> definitions) {
            this.definitions = definitions;
            return this;
        }

        public DefaultRegexCreator build() { return new DefaultRegexCreator(definitions); }
    }
}
