package com.myself.application.mapping.impl;

import com.myself.application.compiler.model.Description;
import com.myself.application.mapping.DefinitionMapper;
import com.myself.application.mapping.FragmentMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.regex.model.Definition;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.myself.application.regex.model.Fragment.*;
import static java.lang.Integer.parseInt;
import static java.util.Objects.requireNonNull;

public class DefaultDefinitionMapper implements DefinitionMapper {

    private final FragmentMapper fragmentMapper;

    public DefaultDefinitionMapper(FragmentMapper fragmentMapper) {
        this.fragmentMapper = requireNonNull(fragmentMapper,
                                             "Fragment mapper should be provided.");
    }

    @Override
    public Definition asDefinition(Expression input) {
        if (input.getValue().matches("")) {
            return new Definition(EPSILON, new int[] {}, "");
        } else if (input.getValue().matches("(^\\w{1}$)")) {
            return new Definition(CHARACTER, new int[] {}, input.getValue());
        } else if (input.getValue().matches("(U),(\\d+),(\\d+)")) {
            Matcher matcher = findMatch("(U),(\\d+),(\\d+)", input.getValue());

            return new Definition(UNION,
                                  new int[] {parseInt(matcher.group(2)), parseInt(matcher.group(3))},
                                  "");
        } else if (input.getValue().matches("(C),(\\d+),(\\d+)")) {
            Matcher matcher = findMatch("(C),(\\d+),(\\d+)", input.getValue());

            return new Definition(CONCATENATION,
                                  new int[] {parseInt(matcher.group(2)), parseInt(matcher.group(3))},
                                  "");
        } else if (input.getValue().matches("(I),(\\d+)")) {
            Matcher matcher = findMatch("(I),(\\d+)", input.getValue());

            return new Definition(ITERATION,
                                  new int[] {parseInt(matcher.group(2))},
                                  "");
        }
        return null; // TODO should throw exception?
    }

    private Matcher findMatch(String inputPattern, String input) {
        Pattern pattern = Pattern.compile(inputPattern);
        Matcher matcher = pattern.matcher(input);
        matcher.find();

        return matcher;
    }

    @Override
    public Definition asDefinition(Description input) {

        return new Definition(fragmentMapper.asFragment(input.getFragment()),
                              input.getIndexes(),
                              input.getValue());
    }
}
