package com.myself.application.service;

import com.myself.application.converter.model.FiniteAutomaton;
import com.myself.application.converter.model.State;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;

public interface ProcessorService {

    /**
     * Reads file and converts it into list of expressions.
     *
     * @param filename the file to be processed
     * @return List fo expressions
     */
    ProcessorResult<Expression> readAutomatonFromFile(String filename);

    /**
     * Writes Finite Automaton to the file.
     *
     * @param finiteAutomaton DFA to be written
     * @param filename        file to be written to
     */
    void writeAutomatonToFile(FiniteAutomaton<State, String> finiteAutomaton, String filename);

}
