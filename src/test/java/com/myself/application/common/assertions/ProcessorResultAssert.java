package com.myself.application.common.assertions;

import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class ProcessorResultAssert extends AbstractAssert<ProcessorResultAssert, ProcessorResult<Expression>> {

    private ProcessorResultAssert(ProcessorResult<Expression> actual) {
        super(actual, ProcessorResultAssert.class);
    }

    public static ProcessorResultAssert assertThat(ProcessorResult<Expression> actual) {
        return new ProcessorResultAssert(actual);
    }

    public ProcessorResultAssert hasValue(int index, String value) {
        isNotNull();

        if (!Objects.equals(actual.getData().get(index).getValue(), value)) {
            failWithMessage("Expected processor result to have value <%s> but had <%s>.",
                            value,
                            actual.getData().get(index).getValue());
        }
        return this;
    }

    public ProcessorResultAssert hasSize(int size) {
        isNotNull();

        if (!Objects.equals(actual.getData().size(), size)) {
            failWithMessage("Expected processor result to have size <%s> but had <%s>.",
                            size,
                            actual.getData().size());
        }
        return this;
    }

}
