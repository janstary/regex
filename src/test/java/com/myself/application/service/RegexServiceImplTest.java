package com.myself.application.service;

import com.myself.application.compiler.Compiler;
import com.myself.application.compiler.DescriptionFactory;
import com.myself.application.compiler.Lexer;
import com.myself.application.compiler.Parser;
import com.myself.application.compiler.TokenFactory;
import com.myself.application.compiler.impl.DefaultDescriptionFactory;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.impl.RegexCompiler;
import com.myself.application.compiler.impl.RegexLexer;
import com.myself.application.compiler.impl.RegexParser;
import com.myself.application.compiler.model.Description;
import com.myself.application.mapping.DefinitionMapper;
import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.mapping.FragmentMapper;
import com.myself.application.mapping.impl.DefaultDefinitionMapper;
import com.myself.application.mapping.impl.DefaultExpressionMapper;
import com.myself.application.mapping.impl.DefaultFragmentMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.service.impl.CompilerServiceImpl;
import com.myself.application.service.impl.ProcessorServiceImpl;
import com.myself.application.service.impl.RegexServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mock;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class RegexServiceImplTest {

    @Mock
    private DefinitionMapper definitionMapper;

    @Mock
    private ExpressionMapper expressionMapper;

    @Mock
    private ProcessorService processorService;

    @Mock
    private CompilerService compilerService;

    @Mock
    private FragmentMapper fragmentMapper;

    private RegexService regexService;

    @BeforeEach
    void setUp() {
        TokenFactory tokenFactory = new DefaultTokenFactory();
        DescriptionFactory descriptionFactory = new DefaultDescriptionFactory();
        Lexer lexer = new RegexLexer(tokenFactory);
        Parser parser = new RegexParser();
        Compiler compiler = new RegexCompiler(lexer, parser, descriptionFactory);

        fragmentMapper = new DefaultFragmentMapper();
        definitionMapper = new DefaultDefinitionMapper(fragmentMapper);
        expressionMapper = new DefaultExpressionMapper();
        processorService = new ProcessorServiceImpl(expressionMapper);
        compilerService = new CompilerServiceImpl(compiler);
        regexService = new RegexServiceImpl(definitionMapper);
    }

    @Test
    void test_01_create_nfa_from_file() {
        // GIVEN
        String filename = "src\\test\\resources\\data1.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));

        // WHEN
        FiniteAutomaton result = regexService.createFA(descriptions);

        // THEN
        assertNotNull(result);
//        assertThat(result).hasStartState("q0")
//                          .hasTransitionsSize(7)
//                          .hasAcceptStatesSize(1)
//                          .hasStatesSize(6);
    }

}
