package com.myself.application.regex.creator.impl;

import com.myself.application.regex.creator.Creator;
import com.myself.application.regex.creator.StateFactory;
import com.myself.application.regex.creator.WrongFormatException;
import com.myself.application.regex.creator.model.Expression;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.regex.creator.model.State;
import com.myself.application.regex.creator.model.Transition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static com.myself.application.regex.creator.model.ExpressionType.STATE;
import static com.myself.application.regex.creator.model.ExpressionType.TRANSITION;
import static java.util.Objects.requireNonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 * Class represents Finite Automaton creator.
 */
public class FACreator implements Creator<FiniteAutomaton> {

    /**
     * Expressions from which finite automaton is made of
     */
    private final List<Expression> expressions;

    private FACreator(List<Expression> expressions) {
        this.expressions = requireNonNull(expressions, "Expressions should be provided.");
    }

    public static FACreatorBuilder builder() {
        return new FACreatorBuilder();
    }

    /**
     * Creates Finite Automaton by parsing expressions.
     *
     * @return Created finite automaton
     * @throws WrongFormatException when automaton is defined by wrong format
     */
    @Override
    public FiniteAutomaton create() throws WrongFormatException {
        Map<String, State> states = createStates(this.expressions);
        Set<Transition> transitions = createTransitions(this.expressions, states);
        State startState = getStartState(states);
        Set<State> acceptStates = getAcceptStates(states);

        return new FiniteAutomaton(startState,
                                   acceptStates,
                                   transitions,
                                   new HashSet<>(states.values()));
    }

    /**
     * Creates states from expressions.
     *
     * @param expressions List of expressions
     * @return States of finite automaton
     */
    private Map<String, State> createStates(List<Expression> expressions) {
        return expressions.stream()
                          .filter(expression -> expression.getType() == STATE)
                          .map(expression -> {
                              String input = expression.getValue().length == 1
                                             ? " "
                                             : expression.getValue()[1];
                              return StateFactory.getState(expression.getValue()[0], input);
                          })
                          .collect(toMap(State::getName, identity()));
    }

    /**
     * Creates transitions from expressions and states.
     *
     * @param expressions List of expressions
     * @param states      States of finite automaton
     * @return Set of all transitions in finite automaton
     * @throws WrongFormatException when transition depends on non existing state
     */
    private Set<Transition> createTransitions(List<Expression> expressions,
                                              Map<String, State> states) throws WrongFormatException {
        return expressions.stream()
                          .filter(expression -> expression.getType() == TRANSITION)
                          .map(expression -> {
                                   State state1 = states.get(expression.getValue()[0]);
                                   State state2 = states.get(expression.getValue()[2]);
                                   if (state1 == null || state2 == null) {
                                       throw new WrongFormatException(
                                               "Wrong format! State in transition was not found.");
                                   }
                                   return new Transition(expression.getValue()[1], state1, state2);
                               }
                          ).collect(toSet());
    }

    /**
     * Filters all accept states of finite automaton.
     *
     * @param states States of finite automaton
     * @return Set of all accept states of finite automaton
     */
    private Set<State> getAcceptStates(Map<String, State> states) {
        return states.entrySet()
                     .stream()
                     .filter(x -> x.getValue().isFinal())
                     .map(Entry::getValue)
                     .collect(toSet());
    }

    /**
     * Finds start state of finite automaton.
     *
     * @param states States of finite automaton
     * @return start state
     * @throws WrongFormatException when no start state was found
     */
    private State getStartState(Map<String, State> states) throws WrongFormatException {
        return states.entrySet()
                     .stream()
                     .filter(entry -> entry.getValue().isInitial())
                     .map(Entry::getValue)
                     .findFirst()
                     .orElseThrow(() -> new WrongFormatException(
                             "Wrong format! No initial state was found."));
    }

    /**
     * Builder of the class.
     */
    public static final class FACreatorBuilder {

        private List<Expression> expressions = new ArrayList<>();

        private FACreatorBuilder() {}

        public FACreatorBuilder expression(Expression expression) {
            if (this.expressions == null) {
                this.expressions = new ArrayList<>();
            }

            this.expressions.add(expression);

            return this;
        }

        public FACreatorBuilder expressions(Collection<Expression> expressions) {
            this.expressions.addAll(expressions);
            return this;
        }

        public FACreator build() { return new FACreator(expressions); }
    }
}
