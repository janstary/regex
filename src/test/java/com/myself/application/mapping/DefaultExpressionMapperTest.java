package com.myself.application.mapping;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import com.myself.application.mapping.impl.DefaultExpressionMapper;
import com.myself.application.regex.creator.model.Expression;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.common.assertions.ExpressionAssert.assertThat;
import static com.myself.application.regex.creator.model.ExpressionType.STATE;
import static com.myself.application.regex.creator.model.ExpressionType.TRANSITION;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultExpressionMapperTest {

    private ExpressionMapper expressionMapper;

    @BeforeEach
    void setUp() {
        this.expressionMapper = new DefaultExpressionMapper();
    }

    @Test
    void test_01_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("q0 F");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(STATE)
                          .hasValue(new String[] {"q0", "F"});
    }

    @Test
    void test_02_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("q12345");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(STATE)
                          .hasValue(new String[] {"q12345"});
    }

    @Test
    void test_03_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("   q123   ");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(STATE)
                          .hasValue(new String[] {"q123"});
    }

    @Test
    void test_04_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("q1   F   ");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(STATE)
                          .hasValue(new String[] {"q1", "F"});
    }

    @Test
    void test_05_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("q1,,q2");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(TRANSITION)
                          .hasValue(new String[] {"q1", "", "q2"});
    }

    @Test
    void test_06_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("q1,char,q2");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(TRANSITION)
                          .hasValue(new String[] {"q1", "char", "q2"});
    }

    @Test
    void test_07_as_expression() {
        // GIVEN
        com.myself.application.processor.model.Expression expression = createExpression("   q1  ,  char  ,  q2  ");

        // WHEN
        Expression result = this.expressionMapper.asExpression(expression);

        // THEN
        assertNotNull(result);
        assertThat(result).hasType(TRANSITION)
                          .hasValue(new String[] {"q1", "char", "q2"});
    }

    @Test
    void test_08_as_expression() {
        // GIVEN
        State state = new State("q0");
        String symbol = "F";

        // WHEN
        com.myself.application.processor.model.Expression result = this.expressionMapper.asExpression(state, symbol);

        // THEN
        assertNotNull(result);
        assertEquals(result.getValue(), format("%s %s", state.getName(), symbol));
    }

    @Test
    void test_09_as_expression() {
        // GIVEN
        State state = new State("q123456");
        String symbol = "IF";

        // WHEN
        com.myself.application.processor.model.Expression result = this.expressionMapper.asExpression(state, symbol);

        // THEN
        assertNotNull(result);
        assertEquals(result.getValue(), format("%s %s", state.getName(), symbol));
    }

    @Test
    void test_10_as_expression() {
        // GIVEN
        State state1 = new State("q0");
        State state2 = new State("q0");
        String symbol = "a";
        Transition<State, String> transition = new Transition<>(symbol,
                                                                state1,
                                                                state2);

        // WHEN
        com.myself.application.processor.model.Expression result = this.expressionMapper.asExpression(transition);

        // THEN
        assertNotNull(result);
        assertEquals(result.getValue(), format("%s,%s,%s",
                                               state1.getName(),
                                               symbol,
                                               state2.getName()));
    }

    @Test
    void test_11_as_expression() {
        // GIVEN
        State state1 = new State("q12345");
        State state2 = new State("q6789");
        String symbol = "just_a_simple_character";
        Transition<State, String> transition = new Transition<>(symbol,
                                                                state1,
                                                                state2);

        // WHEN
        com.myself.application.processor.model.Expression result = this.expressionMapper.asExpression(transition);

        // THEN
        assertNotNull(result);
        assertEquals(result.getValue(), format("%s,%s,%s",
                                               state1.getName(),
                                               symbol,
                                               state2.getName()));
    }

    private com.myself.application.processor.model.Expression createExpression(String value) {
        return new com.myself.application.processor.model.Expression(value);
    }

}
