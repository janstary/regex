package com.myself.application.regex;

/**
 * Is thrown when syntax of the regular expression is incorrect.
 */
public class SyntaxErrorException extends RuntimeException {

    public SyntaxErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxErrorException(String message) {
        super(message);
    }

}
