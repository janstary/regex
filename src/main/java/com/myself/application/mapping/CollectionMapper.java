package com.myself.application.mapping;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Interface maps collections as whole.
 */
public interface CollectionMapper {

    /**
     * @param source         Set to be mapped
     * @param singularMapper Anonymous function applied to mapping
     * @param <S>            Type of input
     * @param <T>            Type of output
     * @return Set containing the entities being mapped
     */
    default <S, T> Set<T> mapSet(Set<S> source, Function<S, T> singularMapper) {
        requireNonNull(singularMapper, "Singular Mapper must not me null.");

        if (source == null) {
            return emptySet();
        }
        return source.stream()
                     .map(singularMapper)
                     .collect(toSet());
    }

    /**
     * @param source         List to be mapped
     * @param singularMapper Anonymous function applied to mapping
     * @param <S>            Type of input
     * @param <T>            Type of output
     * @return List containing the entities being mapped
     */
    default <S, T> List<T> mapList(List<S> source, Function<S, T> singularMapper) {
        requireNonNull(singularMapper, "Singular Mapper must not me null.");

        if (source == null) {
            return emptyList();
        }
        return source.stream()
                     .map(singularMapper)
                     .filter(Objects::nonNull)
                     .collect(toList());
    }

}
