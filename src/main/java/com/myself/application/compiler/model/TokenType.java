package com.myself.application.compiler.model;

public enum TokenType {
    LEFT_BRACKET(0, 0),
    RIGHT_BRACKET(0, 0),
    CHARACTER_AUTOMATON(0, 0),
    EPSILON_AUTOMATON(0, 0),
    ITERATION(1, 5),
    CONCATENATION(2, 5),
    UNION(2, 5);

    /**
     * Represents, how many operands the operation should have
     */
    private final int numOfOperands;
    /**
     * Represents precedence of type
     */
    private final int precedence;

    TokenType(int numOfOperands, int precedence) {
        this.numOfOperands = numOfOperands;
        this.precedence = precedence;
    }

    public int getNumOfOperands() {
        return numOfOperands;
    }

    public int getPrecedence() {
        return precedence;
    }
}
