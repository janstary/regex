package com.myself.application.service;

import com.myself.application.compiler.model.Description;
import com.myself.application.processor.model.Expression;

import java.util.List;

public interface CompilerService {

    List<Description> compile(Expression expression);

}
