package com.myself.application.compiler;

import com.myself.application.common.assertions.DescriptionAssert;
import com.myself.application.compiler.impl.DefaultDescriptionFactory;
import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.Fragment;
import com.myself.application.compiler.model.OperandNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.compiler.model.Fragment.CHARACTER;
import static com.myself.application.compiler.model.Fragment.EPSILON;
import static com.myself.application.compiler.model.TokenType.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultDescriptionFactoryTest {

    private DescriptionFactory descriptionFactory;

    @BeforeEach
    void setUp() {
        descriptionFactory = new DefaultDescriptionFactory();
    }

    @Test
    void test_01_success() {
        // GIVEN
        OperandNode node = OperandNode.builder()
                                      .index(1)
                                      .value("a")
                                      .operation(CHARACTER_AUTOMATON)
                                      .build();

        // WHEN
        Description result = descriptionFactory.createDescription(node);

        // THEN
        assertNotNull(result);
        DescriptionAssert.assertThat(result)
                         .hasValue("a")
                         .hasOperation(CHARACTER);
    }

    @Test
    void test_02_success() {
        // GIVEN
        OperandNode node = OperandNode.builder()
                                      .index(1)
                                      .value("|")
                                      .operation(UNION)
                                      .build();

        // WHEN
        Description result = descriptionFactory.createDescription(node);

        // THEN
        assertNotNull(result);
        DescriptionAssert.assertThat(result)
                         .hasValue("|")
                         .hasIndex(0, 0)
                         .hasIndex(1, 0)
                         .hasIndexLength(2)
                         .hasOperation(Fragment.UNION);
    }

    @Test
    void test_03_success() {
        // GIVEN
        OperandNode node = OperandNode.builder()
                                      .index(1)
                                      .value("*")
                                      .operation(ITERATION)
                                      .build();

        // WHEN
        Description result = descriptionFactory.createDescription(node);

        // THEN
        assertNotNull(result);
        DescriptionAssert.assertThat(result)
                         .hasValue("*")
                         .hasIndex(0, 0)
                         .hasIndexLength(1)
                         .hasOperation(Fragment.ITERATION);
    }

    @Test
    void test_04_success() {
        // GIVEN
        OperandNode node = OperandNode.builder()
                                      .index(1)
                                      .value(" ")
                                      .operation(EPSILON_AUTOMATON)
                                      .build();

        // WHEN
        Description result = descriptionFactory.createDescription(node);

        // THEN
        assertNotNull(result);
        DescriptionAssert.assertThat(result)
                         .hasValue(" ")
                         .hasOperation(EPSILON);
    }

    @Test
    void test_05_success() {
        // GIVEN
        OperandNode node = OperandNode.builder()
                                      .index(1)
                                      .value(".")
                                      .operation(CONCATENATION)
                                      .build();

        // WHEN
        Description result = descriptionFactory.createDescription(node);

        // THEN
        assertNotNull(result);
        DescriptionAssert.assertThat(result)
                         .hasValue(".")
                         .hasIndex(0, 0)
                         .hasIndex(1, 0)
                         .hasIndexLength(2)
                         .hasOperation(Fragment.CONCATENATION);
    }

}
