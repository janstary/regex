package com.myself.application.regex.creator;

/**
 * Exception is thrown when automaton is defined by wrong format. For example,
 * when no initial state is defined or transition depends on non existing state.
 */
public class WrongFormatException extends RuntimeException {

    public WrongFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongFormatException(String message) {
        super(message);
    }
}
