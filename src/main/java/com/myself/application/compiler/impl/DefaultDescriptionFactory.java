package com.myself.application.compiler.impl;

import com.myself.application.compiler.DescriptionFactory;
import com.myself.application.compiler.exception.NoSuchElementException;
import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.OperandNode;

import static com.myself.application.compiler.model.Fragment.*;

/**
 * Class is responsible for description creation
 */
public class DefaultDescriptionFactory implements DescriptionFactory {

    @Override
    public Description createDescription(OperandNode node) throws NoSuchElementException {
        int left = node.getLeft() != null ? node.getLeft().getIndex() : 0;
        int right = node.getRight() != null ? node.getRight().getIndex() : 0;

        switch (node.getOperation()) {
            case CHARACTER_AUTOMATON:
                return new Description(node.getValue(), CHARACTER, new int[] {});
            case EPSILON_AUTOMATON:
                return new Description(node.getValue(), EPSILON, new int[] {});
            case UNION:
                return new Description(node.getValue(), UNION, new int[] {left, right});
            case ITERATION:
                return new Description(node.getValue(), ITERATION, new int[] {left});
            case CONCATENATION:
                return new Description(node.getValue(), CONCATENATION, new int[] {left, right});
        }
        throw new NoSuchElementException("No such Description!");
    }
}
