package com.myself.application.mapping;

import com.myself.application.common.assertions.DefinitionAssert;
import com.myself.application.mapping.impl.DefaultDefinitionMapper;
import com.myself.application.mapping.impl.DefaultFragmentMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.regex.model.Definition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.regex.model.Fragment.*;

@TestMethodOrder(Alphanumeric.class)
public class DefaultDescriptionMapperTest {

    private DefinitionMapper definitionMapper;

    @BeforeEach
    void setUp() {
        FragmentMapper fragmentMapper = new DefaultFragmentMapper();
        this.definitionMapper = new DefaultDefinitionMapper(fragmentMapper);
    }

    @Test
    void test_01_asDefinition() {
        // GIVEN
        Expression expression = new Expression("a");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(CHARACTER)
                        .hasSymbol("a");
    }

    @Test
    void test_02_asDefinition() {
        // GIVEN
        Expression expression = new Expression("y");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(CHARACTER)
                        .hasSymbol("y");
    }

    @Test
    void test_03_asDefinition() {
        // GIVEN
        Expression expression = new Expression("");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(EPSILON)
                        .hasSymbol("");
    }

    @Test
    void test_04_asDefinition() {
        // GIVEN
        Expression expression = new Expression("U,1,2");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(UNION)
                        .hasSymbol("")
                        .hasIndexLength(2)
                        .hasIndex(0, 1)
                        .hasIndex(1, 2);
    }

    @Test
    void test_05_asDefinition() {
        // GIVEN
        Expression expression = new Expression("U,123,234");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(UNION)
                        .hasSymbol("")
                        .hasIndexLength(2)
                        .hasIndex(0, 123)
                        .hasIndex(1, 234);
    }

    @Test
    void test_06_asDefinition() {
        // GIVEN
        Expression expression = new Expression("I,1");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(ITERATION)
                        .hasSymbol("")
                        .hasIndexLength(1)
                        .hasIndex(0, 1);
    }

    @Test
    void test_07_asDefinition() {
        // GIVEN
        Expression expression = new Expression("I,123");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(ITERATION)
                        .hasSymbol("")
                        .hasIndexLength(1)
                        .hasIndex(0, 123);
    }

    @Test
    void test_08_asDefinition() {
        // GIVEN
        Expression expression = new Expression("C,1,2");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(CONCATENATION)
                        .hasSymbol("")
                        .hasIndexLength(2)
                        .hasIndex(0, 1)
                        .hasIndex(1, 2);
    }

    @Test
    void test_09_asDefinition() {
        // GIVEN
        Expression expression = new Expression("C,123,234");

        // WHEN
        Definition result = this.definitionMapper.asDefinition(expression);

        // THEN
        DefinitionAssert.assertThat(result)
                        .hasFragment(CONCATENATION)
                        .hasSymbol("")
                        .hasIndexLength(2)
                        .hasIndex(0, 123)
                        .hasIndex(1, 234);
    }

}
