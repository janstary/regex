package com.myself.application.regex.creator.model;

/**
 * Represents type of expression. In other words tells, whether it is STATE or TRANSITION.
 */
public enum ExpressionType {
    STATE,
    TRANSITION
}
