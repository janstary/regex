package com.myself.application.common.assertions;

import com.myself.application.regex.model.Definition;
import com.myself.application.regex.model.Fragment;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class DefinitionAssert extends AbstractAssert<DefinitionAssert, Definition> {

    private DefinitionAssert(Definition actual) {
        super(actual, DefinitionAssert.class);
    }

    public static DefinitionAssert assertThat(Definition actual) {
        return new DefinitionAssert(actual);
    }

    public DefinitionAssert hasFragment(Fragment fragment) {
        isNotNull();

        if (!Objects.equals(actual.getFragment(), fragment)) {
            failWithMessage("Expected definition to have fragment <%s> but had <%s>.",
                            fragment,
                            actual.getFragment());
        }
        return this;
    }

    public DefinitionAssert hasSymbol(String symbol) {
        isNotNull();

        if (!Objects.equals(actual.getSymbol(), symbol)) {
            failWithMessage("Expected definition to have symbol <%s> but had <%s>.",
                            symbol,
                            actual.getSymbol());
        }
        return this;
    }

    public DefinitionAssert hasIndex(int index, int value) {
        isNotNull();

        if (!Objects.equals(actual.getIndexes()[index], value)) {
            failWithMessage("Expected definition to have index[%s] <%s> but had <%s>.",
                            index,
                            value,
                            actual.getIndexes()[index]);
        }
        return this;
    }

    public DefinitionAssert hasIndexLength(int length) {
        isNotNull();

        if (!Objects.equals(actual.getIndexes().length, length)) {
            failWithMessage("Expected definition to have index length <%s> but had <%s>.",
                            length,
                            actual.getIndexes().length);
        }
        return this;
    }

}
