package com.myself.application.compiler.model;

import java.util.Objects;

/**
 *
 */
public class OperandNode {

    private final String value;
    private final TokenType operation;
    private final int index;
    private OperandNode left;
    private OperandNode right;

    private OperandNode(int index,
                        String value,
                        TokenType operation,
                        OperandNode left,
                        OperandNode right) {
        this.index = index;
        this.value = value;
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    public static OperandNodeBuilder builder() {
        return new OperandNodeBuilder();
    }

    public String getValue() {
        return value;
    }

    public TokenType getOperation() {
        return operation;
    }

    public int getIndex() {
        return index;
    }

    public OperandNode getLeft() {
        return left;
    }

    public OperandNode getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OperandNode node = (OperandNode) o;
        return index == node.index &&
               Objects.equals(value, node.value) &&
               operation == node.operation &&
               Objects.equals(left, node.left) &&
               Objects.equals(right, node.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, operation, index, left, right);
    }

    /**
     * Builder of the class
     */
    public static final class OperandNodeBuilder {

        private String value;
        private TokenType operation;
        private int index;
        private OperandNode left;
        private OperandNode right;

        private OperandNodeBuilder() {}

        public static OperandNodeBuilder anOperandNode() { return new OperandNodeBuilder(); }

        public OperandNodeBuilder value(String value) {
            this.value = value;
            return this;
        }

        public OperandNodeBuilder operation(TokenType operation) {
            this.operation = operation;
            return this;
        }

        public OperandNodeBuilder index(int index) {
            this.index = index;
            return this;
        }

        public OperandNodeBuilder left(OperandNode left) {
            this.left = left;
            return this;
        }

        public OperandNodeBuilder right(OperandNode right) {
            this.right = right;
            return this;
        }

        public OperandNode build() { return new OperandNode(index, value, operation, left, right); }
    }
}
