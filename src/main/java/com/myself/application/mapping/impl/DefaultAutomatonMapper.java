package com.myself.application.mapping.impl;

import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;
import com.myself.application.mapping.AutomatonMapper;
import com.myself.application.mapping.StateMapper;
import com.myself.application.mapping.TransitionMapper;
import com.myself.application.regex.creator.model.FiniteAutomaton;

import static java.util.Objects.requireNonNull;

public class DefaultAutomatonMapper implements AutomatonMapper<State, String> {

    private final StateMapper stateMapper;
    private final TransitionMapper transitionMapper;

    public DefaultAutomatonMapper(StateMapper stateMapper, TransitionMapper transitionMapper) {
        this.stateMapper = requireNonNull(stateMapper, "State mapper should be provided.");
        this.transitionMapper = requireNonNull(transitionMapper,
                                               "Transition mapper should be provided.");
    }

    @Override
    public NFA<State, String> asNFA(FiniteAutomaton input) {
        return new NFA<>(stateMapper.asState(input.getStartState()),
                         stateMapper.mapSet(input.getAcceptStates(), stateMapper::asState),
                         transitionMapper.mapSet(input.getTransitions(),
                                                 transitionMapper::asTransition),
                         stateMapper.mapSet(input.getStates(), stateMapper::asState));
    }

    @Override
    public DFA<State, String> asDFA(FiniteAutomaton input) {
        return new DFA<>(stateMapper.asState(input.getStartState()),
                         stateMapper.mapSet(input.getAcceptStates(),
                                            stateMapper::asState),
                         transitionMapper.mapSet(input.getTransitions(),
                                                 transitionMapper::asTransition),
                         stateMapper.mapSet(input.getStates(), stateMapper::asState));
    }
}
