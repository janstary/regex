package com.myself.application.processor.model;

import java.util.Objects;

/**
 * Class represents one line in file.
 */
public class Expression {

    /**
     * Represents value of the line. In other words, it is raw unparsed value.
     */
    private final String value;

    public Expression(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Expression that = (Expression) o;
        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
