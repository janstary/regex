package com.myself.application.mapping.impl;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import com.myself.application.mapping.StateMapper;
import com.myself.application.mapping.TransitionMapper;

import static java.util.Objects.requireNonNull;

public class DefaultTransitionMapper implements TransitionMapper {

    private final StateMapper stateMapper;

    public DefaultTransitionMapper(StateMapper stateMapper) {
        this.stateMapper = requireNonNull(stateMapper, "State mapper should be provided.");
    }

    @Override
    public Transition<State, String> asTransition(
            com.myself.application.regex.creator.model.Transition input) {
        return new Transition<>(input.getSymbol(),
                                stateMapper.asState(input.getFrom()),
                                stateMapper.asState(input.getTo()));
    }
}
