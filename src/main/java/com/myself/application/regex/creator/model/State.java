package com.myself.application.regex.creator.model;

import java.util.Objects;

/**
 * Class represents one specific state in final automata.
 */
public class State {

    /**
     * Name of the state, should be unique
     */
    private final String name;
    /**
     * Tells if is state final or not
     */
    private final boolean isFinal;
    /**
     * Tells if is state initial or not
     */
    private final boolean isInitial;

    public State(String name, boolean isFinal, boolean isInitial) {
        this.name = name;
        this.isFinal = isFinal;
        this.isInitial = isInitial;
    }

    public String getName() {
        return name;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public boolean isInitial() {
        return isInitial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        State state = (State) o;
        return isFinal == state.isFinal &&
               isInitial == state.isInitial &&
               Objects.equals(name, state.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, isFinal, isInitial);
    }
    
}
