package com.myself.application.mapping.impl;

import com.myself.application.compiler.model.Fragment;
import com.myself.application.mapping.FragmentMapper;

public class DefaultFragmentMapper implements FragmentMapper {

    @Override
    public com.myself.application.regex.model.Fragment asFragment(Fragment input) {
        switch (input) {
            case CHARACTER:
                return com.myself.application.regex.model.Fragment.CHARACTER;
            case CONCATENATION:
                return com.myself.application.regex.model.Fragment.CONCATENATION;
            case ITERATION:
                return com.myself.application.regex.model.Fragment.ITERATION;
            case UNION:
                return com.myself.application.regex.model.Fragment.UNION;
            case EPSILON:
                return com.myself.application.regex.model.Fragment.EPSILON;
        }

        return null; // TODO throw exception instead
    }
}
