package com.myself.application;

import com.myself.application.compiler.Compiler;
import com.myself.application.compiler.DescriptionFactory;
import com.myself.application.compiler.Lexer;
import com.myself.application.compiler.Parser;
import com.myself.application.compiler.TokenFactory;
import com.myself.application.compiler.impl.DefaultDescriptionFactory;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.impl.RegexCompiler;
import com.myself.application.compiler.impl.RegexLexer;
import com.myself.application.compiler.impl.RegexParser;
import com.myself.application.compiler.model.Description;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.State;
import com.myself.application.mapping.AutomatonMapper;
import com.myself.application.mapping.DefinitionMapper;
import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.mapping.FragmentMapper;
import com.myself.application.mapping.StateMapper;
import com.myself.application.mapping.TransitionMapper;
import com.myself.application.mapping.impl.DefaultAutomatonMapper;
import com.myself.application.mapping.impl.DefaultDefinitionMapper;
import com.myself.application.mapping.impl.DefaultExpressionMapper;
import com.myself.application.mapping.impl.DefaultFragmentMapper;
import com.myself.application.mapping.impl.DefaultStateMapper;
import com.myself.application.mapping.impl.DefaultTransitionMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.service.CompilerService;
import com.myself.application.service.ConverterService;
import com.myself.application.service.ProcessorService;
import com.myself.application.service.RegexService;
import com.myself.application.service.impl.CompilerServiceImpl;
import com.myself.application.service.impl.ConverterServiceImpl;
import com.myself.application.service.impl.ProcessorServiceImpl;
import com.myself.application.service.impl.RegexServiceImpl;

import java.util.List;
import java.util.Scanner;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class Application {

    public static void main(String[] args) {

        if (args.length < 2) {
            System.err.println(
                    "Wrong number of arguments. Please provide correct number of arguments");
            return;
        }

        // --------------------------------- COMPILER --------------------------------------
        TokenFactory tokenFactory = new DefaultTokenFactory();
        DescriptionFactory descriptionFactory = new DefaultDescriptionFactory();
        Lexer lexer = new RegexLexer(tokenFactory);
        Parser parser = new RegexParser();
        Compiler compiler = new RegexCompiler(lexer, parser, descriptionFactory);

        // --------------------------------- MAPPERS --------------------------------------
        StateMapper stateMapper = new DefaultStateMapper();
        TransitionMapper transitionMapper = new DefaultTransitionMapper(stateMapper);
        AutomatonMapper<State, String> automatonMapper = new DefaultAutomatonMapper(stateMapper,
                                                                                    transitionMapper);
        FragmentMapper fragmentMapper = new DefaultFragmentMapper();
        DefinitionMapper definitionMapper = new DefaultDefinitionMapper(fragmentMapper);
        ExpressionMapper expressionMapper = new DefaultExpressionMapper();

        // --------------------------------- SERVICES --------------------------------------
        ProcessorService processorService = new ProcessorServiceImpl(expressionMapper);
        RegexService regexService = new RegexServiceImpl(definitionMapper);
        ConverterService converterService = new ConverterServiceImpl();
        CompilerService compilerService = new CompilerServiceImpl(compiler);

        // read file and process it
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(args[0]);

        // compile regex expression to list of descriptions
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));

        // create automaton from descriptions
        FiniteAutomaton finiteAutomaton = regexService.createFA(descriptions);

        // convert finite automaton to dfa
        DFA<State, String> dfa = converterService.convertNFAToDFA(automatonMapper.asNFA(finiteAutomaton));

        // write converted automaton to file
        processorService.writeAutomatonToFile(dfa, args[1]);

        System.out.print("Please provide an input: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        System.out.println(format("Does automata accept '%s': %s",
                                  input,
                                  dfa.accepts(asList(input.split("")))));

    }

}
