package com.myself.application.regex;

import com.myself.application.regex.creator.model.FiniteAutomaton;

/**
 * Interface represents regular expression creator.
 */
public interface RegexCreator {

    /**
     * Creates finite automaton from regular expressions given in specific format.
     *
     * @return Created finite automaton
     * @throws SyntaxErrorException when syntax of the regular expression is incorrect
     */
    FiniteAutomaton create() throws SyntaxErrorException;

}
