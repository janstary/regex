package com.myself.application.mapping;

import com.myself.application.mapping.impl.DefaultStateMapper;
import com.myself.application.regex.creator.model.State;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultStateMapperTest {

    private StateMapper stateMapper;

    @BeforeEach
    void setUp() {
        this.stateMapper = new DefaultStateMapper();
    }

    @Test
    void test_01_as_state() {
        // GIVEN
        State state = new State("stateName", true, true);

        // WHEN
        com.myself.application.converter.model.State result = this.stateMapper.asState(state);

        // THEN
        assertNotNull(result);
        assertEquals(result.getName(), state.getName());
    }

}
