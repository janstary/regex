package com.myself.application.mapping;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;

/**
 * Maps transition from creator package to transition from converter
 */
public interface TransitionMapper extends CollectionMapper {

    /**
     * Takes input of one specific type and produce output of another specific type
     *
     * @param input entity to be mapped
     * @return Mapped entity
     */
    Transition<State, String> asTransition(com.myself.application.regex.creator.model.Transition input);

}
