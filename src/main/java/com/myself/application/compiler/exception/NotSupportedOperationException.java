package com.myself.application.compiler.exception;

/**
 *
 */
public class NotSupportedOperationException extends RuntimeException {

    public NotSupportedOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportedOperationException(String message) {
        super(message);
    }

}
