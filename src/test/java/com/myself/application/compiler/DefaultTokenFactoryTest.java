package com.myself.application.compiler;

import com.myself.application.common.assertions.TokenAssert;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.model.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.compiler.model.TokenType.CHARACTER_AUTOMATON;
import static com.myself.application.compiler.model.TokenType.EPSILON_AUTOMATON;
import static com.myself.application.compiler.model.TokenType.ITERATION;
import static com.myself.application.compiler.model.TokenType.UNION;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultTokenFactoryTest {

    private TokenFactory tokenFactory;

    @BeforeEach
    void setUp() {
        tokenFactory = new DefaultTokenFactory();
    }

    @Test
    void test_01_success() {
        // GIVEN
        String value = "a";

        // WHEN
        Token result = tokenFactory.createToken(value);

        // THEN
        assertNotNull(result);
        TokenAssert.assertThat(result)
                   .hasSymbol("a")
                   .hasType(CHARACTER_AUTOMATON);
    }

    @Test
    void test_02_success() {
        // GIVEN
        String value = " ";

        // WHEN
        Token result = tokenFactory.createToken(value);

        // THEN
        assertNotNull(result);
        TokenAssert.assertThat(result)
                   .hasSymbol(" ")
                   .hasType(EPSILON_AUTOMATON);
    }

    @Test
    void test_03_success() {
        // GIVEN
        String value = "*";

        // WHEN
        Token result = tokenFactory.createToken(value);

        // THEN
        assertNotNull(result);
        TokenAssert.assertThat(result)
                   .hasSymbol("*")
                   .hasType(ITERATION);
    }

    @Test
    void test_04_success() {
        // GIVEN
        String value = "|";

        // WHEN
        Token result = tokenFactory.createToken(value);

        // THEN
        assertNotNull(result);
        TokenAssert.assertThat(result)
                   .hasSymbol("|")
                   .hasType(UNION);
    }

    @Test
    void test_05_success() {
        // GIVEN
        String value = "a";

        // WHEN
        Token result = tokenFactory.createToken(value);

        // THEN
        assertNotNull(result);
        TokenAssert.assertThat(result)
                   .hasSymbol("a")
                   .hasType(CHARACTER_AUTOMATON);
    }

}
