package com.myself.application.common.assertions;

import com.myself.application.compiler.model.Description;
import com.myself.application.compiler.model.Fragment;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class DescriptionAssert extends AbstractAssert<DescriptionAssert, Description> {

    private DescriptionAssert(Description actual) {
        super(actual, DescriptionAssert.class);
    }

    public static DescriptionAssert assertThat(Description actual) {
        return new DescriptionAssert(actual);
    }

    public DescriptionAssert hasOperation(Fragment fragment) {
        isNotNull();

        if (!Objects.equals(actual.getFragment(), fragment)) {
            failWithMessage("Expected description to have fragment <%s> but had <%s>.",
                            fragment,
                            actual.getFragment());
        }
        return this;
    }

    public DescriptionAssert hasValue(String value) {
        isNotNull();

        if (!Objects.equals(actual.getValue(), value)) {
            failWithMessage("Expected description to have symbol <%s> but had <%s>.",
                            value,
                            actual.getValue());
        }
        return this;
    }

    public DescriptionAssert hasIndex(int index, int value) {
        isNotNull();

        if (!Objects.equals(actual.getIndexes()[index], value)) {
            failWithMessage("Expected description to have index[%s] <%s> but had <%s>.",
                            index,
                            value,
                            actual.getIndexes()[index]);
        }
        return this;
    }

    public DescriptionAssert hasIndexLength(int length) {
        isNotNull();

        if (!Objects.equals(actual.getIndexes().length, length)) {
            failWithMessage("Expected description to have index length <%s> but had <%s>.",
                            length,
                            actual.getIndexes().length);
        }
        return this;
    }

}
