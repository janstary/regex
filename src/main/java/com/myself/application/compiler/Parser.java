package com.myself.application.compiler;

import com.myself.application.compiler.model.AST;
import com.myself.application.compiler.model.OperandNode;
import com.myself.application.compiler.model.Token;

import java.util.List;

/**
 * Interface represents parser
 */
public interface Parser {

    /**
     * Parses the input according to input tokens
     *
     * @param tokens sequence of tokens produced by lexer
     * @return List of Description
     */
    AST<OperandNode> parse(List<Token> tokens);

}
