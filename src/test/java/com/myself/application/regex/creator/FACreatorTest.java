package com.myself.application.regex.creator;

import com.myself.application.regex.creator.impl.FACreator;
import com.myself.application.regex.creator.model.Expression;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static com.myself.application.common.assertions.FiniteAutomataAssert.assertThat;
import static com.myself.application.regex.creator.model.ExpressionType.STATE;
import static com.myself.application.regex.creator.model.ExpressionType.TRANSITION;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(Alphanumeric.class)
public class FACreatorTest {

    @Test
    void test_01_success() {
        // GIVEN
        List<Expression> expressions = asList(
                new Expression(STATE, new String[] {"q0", "IF"}),
                new Expression(STATE, new String[] {"q1", "F"}),
                new Expression(TRANSITION, new String[] {"q0", "a", "q1"}));

        // WHEN
        FiniteAutomaton result = FACreator.builder()
                                          .expressions(expressions)
                                          .build()
                                          .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(2)
                          .hasStatesSize(2)
                          .hasTransitionsSize(1)
                          .hasStartState("q0");
    }

    @Test
    void test_02_success() {
        // GIVEN
        List<Expression> expressions = asList(
                new Expression(STATE, new String[] {"q0", "IF"}),
                new Expression(STATE, new String[] {"q1", "F"}),
                new Expression(STATE, new String[] {"q2"}),
                new Expression(STATE, new String[] {"q12345678"}),
                new Expression(TRANSITION, new String[] {"q0", "a", "q1"}));

        // WHEN
        FiniteAutomaton result = FACreator.builder()
                                          .expressions(expressions)
                                          .build()
                                          .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(2)
                          .hasStatesSize(4)
                          .hasTransitionsSize(1)
                          .hasStartState("q0");
    }

    @Test
    void test_03_success() {
        // GIVEN
        List<Expression> expressions = asList(
                new Expression(STATE, new String[] {"q0", "F"}),
                new Expression(STATE, new String[] {"q1", "F"}),
                new Expression(STATE, new String[] {"q2", "I"}),
                new Expression(TRANSITION, new String[] {"q0", "a", "q1"}),
                new Expression(TRANSITION, new String[] {"q0", "b", "q1"}),
                new Expression(TRANSITION, new String[] {"q0", "a", "q2"}),
                new Expression(TRANSITION, new String[] {"q1", "a", "q1"}));

        // WHEN
        FiniteAutomaton result = FACreator.builder()
                                          .expressions(expressions)
                                          .build()
                                          .create();

        // THEN
        assertNotNull(result);
        assertThat(result).hasAcceptStatesSize(2)
                          .hasStatesSize(3)
                          .hasTransitionsSize(4)
                          .hasStartState("q2");
    }

    /**
     * No initial state. Test method should throw WrongFormatException.
     */
    @Test
    void test_04_failure() {
        // GIVEN
        List<Expression> expressions = asList(
                new Expression(STATE, new String[] {"q0", "F"}),
                new Expression(STATE, new String[] {"q1", "F"}),
                new Expression(TRANSITION, new String[] {"q1", "a", "q1"}));

        // WHEN
        WrongFormatException thrown = assertThrows(WrongFormatException.class,
                                                   () -> FACreator.builder()
                                                                  .expressions(expressions)
                                                                  .build()
                                                                  .create(),
                                                   "Expected create() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Wrong format! No initial state was found.", thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

    /**
     * No such state in transition. Test method should throw WrongFormatException.
     */
    @Test
    void test_05_failure() {
        // GIVEN
        List<Expression> expressions = asList(
                new Expression(STATE, new String[] {"q0", "I"}),
                new Expression(STATE, new String[] {"q1", "F"}),
                new Expression(TRANSITION, new String[] {"q12345", "a", "q12345"}));

        // WHEN
        WrongFormatException thrown = assertThrows(WrongFormatException.class,
                                                   () -> FACreator.builder()
                                                                  .expressions(expressions)
                                                                  .build()
                                                                  .create(),
                                                   "Expected create() to throw, but it did not");

        // THEN
        assertNotNull(thrown);
        assertEquals("Wrong format! State in transition was not found.", thrown.getMessage());
        assertTrue(!thrown.getMessage().isEmpty());
    }

}
