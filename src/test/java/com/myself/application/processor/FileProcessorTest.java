package com.myself.application.processor;

import com.myself.application.processor.impl.FileProcessor;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static com.myself.application.common.assertions.ProcessorResultAssert.assertThat;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class FileProcessorTest {

    @Test
    void test_01_success() {
        // GIVEN
        String filename = "src\\test\\resources\\data1.txt";

        // WHEN
        ProcessorResult<Expression> result = FileProcessor.of(filename)
                                                          .read();

        //THEN
        assertNotNull(result);
        assertThat(result).hasSize(1)
                          .hasValue(0, "((a(a*)b)|(a(b*)b))");
    }

    @Test
    void test_02_success() {
        // GIVEN
        String filename = "src\\test\\resources\\test.txt";

        // WHEN
        FileProcessor.of(filename)
                     .data(singletonList(new Expression("")))
                     .write();

        ProcessorResult<Expression> result = FileProcessor.of(filename)
                                                          .read();

        //THEN
        assertNotNull(result);
        assertThat(result).hasSize(1);
    }

}
