package com.myself.application.mapping;

import com.myself.application.compiler.model.Fragment;

public interface FragmentMapper {

    com.myself.application.regex.model.Fragment asFragment(Fragment input);

}
