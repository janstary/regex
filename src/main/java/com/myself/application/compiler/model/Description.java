package com.myself.application.compiler.model;

import java.util.Arrays;
import java.util.Objects;

public class Description {

    private final String value;
    private final Fragment fragment;
    private final int[] indexes;

    public Description(String value, Fragment fragment, int[] indexes) {
        this.value = value;
        this.fragment = fragment;
        this.indexes = indexes;
    }

    public String getValue() {
        return value;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public int[] getIndexes() {
        return indexes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Description that = (Description) o;
        return Objects.equals(value, that.value) &&
               Objects.equals(fragment, that.fragment) &&
               Arrays.equals(indexes, that.indexes);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(value, fragment);
        result = 31 * result + Arrays.hashCode(indexes);
        return result;
    }
}
