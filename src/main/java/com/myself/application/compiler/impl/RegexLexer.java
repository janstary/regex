package com.myself.application.compiler.impl;

import com.myself.application.compiler.Lexer;
import com.myself.application.compiler.TokenFactory;
import com.myself.application.compiler.model.Token;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Class represents lexer of regex grammar
 * Class is responsible for:
 * <ul>
 * <li>
 * read symbols of some alphabet from their input.
 * </li>
 *
 * <li>
 * analyse these symbols and try to match them with the grammar of the language they understood.
 * </li>
 *
 * <li>
 * attach some additional meaning (data) to the recognized elements.
 * </li>
 * </ul>
 */
public class RegexLexer implements Lexer {

    private final TokenFactory tokenFactory;

    public RegexLexer(TokenFactory tokenFactory) {
        this.tokenFactory = requireNonNull(tokenFactory,
                                           "Token factory should be provided.");
    }

    /**
     * Scans the input and produces the matching tokens.
     *
     * @param expression string value to be parsed
     * @return List of tokens
     */
    @Override
    public List<Token> tokenize(String expression) {

        StringBuilder builder = new StringBuilder();
        String lastLetter = "(";

        for (String letter : expression.split("")) {
            if ((!letter.equals("*") && !letter.equals("|") && !letter.equals(")")) &&
                (!lastLetter.equals("(") && !lastLetter.equals("|"))) {
                builder.append(".");
            }

            builder.append(letter);
            lastLetter = letter;
        }

        return Stream.of(builder.toString().split(""))
                     .map(tokenFactory::createToken)
                     .collect(toList());
    }
}
