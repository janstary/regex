package com.myself.application.compiler;

import com.myself.application.compiler.exception.NoSuchElementException;
import com.myself.application.compiler.model.Token;

/**
 * Factory used for creating Token instances
 */
public interface TokenFactory {

    /**
     * Creates token based on specific input value
     *
     * @param value specific input value
     * @return created Token
     * @throws NoSuchElementException
     */
    Token createToken(String value) throws NoSuchElementException;

}
