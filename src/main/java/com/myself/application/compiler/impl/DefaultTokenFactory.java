package com.myself.application.compiler.impl;

import com.myself.application.compiler.TokenFactory;
import com.myself.application.compiler.exception.NoSuchElementException;
import com.myself.application.compiler.model.Token;

import static com.myself.application.compiler.model.TokenType.*;

/**
 * Class is responsible for token creation
 */
public class DefaultTokenFactory implements TokenFactory {

    // TODO maybe default should throw exception and return value for CHARACTER_AUTOMATON should be handled in better way
    @Override
    public Token createToken(String value) throws NoSuchElementException {
        switch (value) {
            case "(":
                return new Token(LEFT_BRACKET, value);
            case ")":
                return new Token(RIGHT_BRACKET, value);
            case "*":
                return new Token(ITERATION, value);
            case "|":
                return new Token(UNION, value);
            case ".":
                return new Token(CONCATENATION, value);
            case " ":
                return new Token(EPSILON_AUTOMATON, value);
            default:
                return new Token(CHARACTER_AUTOMATON, value);
        }
    }
}
