package com.myself.application.regex.creator;

/**
 * Interface represents creator of entity of one specific type.
 *
 * @param <Q> Type of entity to create
 */
public interface Creator<Q> {

    /**
     * Creates entity of specific type.
     *
     * @return Created entity
     * @throws WrongFormatException when entity is defined by wrong format
     */
    Q create() throws WrongFormatException;

}
