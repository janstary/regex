package com.myself.application.converter.model;

import java.util.Objects;

/**
 * Class represents one specific state in finite automata.
 */
public class State {

    /**
     * Name of the state, should be unique
     */
    private final String name;

    public State(String name) {
        this.name = name;
    }

    public static StateBuilder builder() { return new StateBuilder(); }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        State state = (State) o;
        return Objects.equals(name, state.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * Builder of the class
     */
    public static final class StateBuilder {

        private String name;

        private StateBuilder() {}

        public StateBuilder name(String name) {
            this.name = name;
            return this;
        }

        public State build() { return new State(name); }
    }
}
