package com.myself.application.service.impl;

import com.myself.application.compiler.Compiler;
import com.myself.application.compiler.model.Description;
import com.myself.application.processor.model.Expression;
import com.myself.application.service.CompilerService;

import java.util.List;
import java.util.Objects;

public class CompilerServiceImpl implements CompilerService {

    private final Compiler compiler;

    public CompilerServiceImpl(Compiler compiler) {
        this.compiler = Objects.requireNonNull(compiler, "Compiler should be provided.");
    }

    @Override
    public List<Description> compile(Expression expression) {
        return compiler.compile(expression.getValue());
    }
}
