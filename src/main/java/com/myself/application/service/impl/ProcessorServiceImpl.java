package com.myself.application.service.impl;

import com.myself.application.converter.model.FiniteAutomaton;
import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.processor.impl.FileProcessor;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import com.myself.application.service.ProcessorService;

import java.util.LinkedList;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class ProcessorServiceImpl implements ProcessorService {

    private final ExpressionMapper expressionMapper;

    public ProcessorServiceImpl(ExpressionMapper expressionMapper) {
        this.expressionMapper = requireNonNull(expressionMapper,
                                               "Expression mapper should be provided.");
    }

    @Override
    public ProcessorResult<Expression> readAutomatonFromFile(String filename) {
        return FileProcessor.of(filename)
                            .read();
    }

    @Override
    public void writeAutomatonToFile(FiniteAutomaton<State, String> finiteAutomaton,
                                     String filename) {
        List<Expression> expressions = automatonToExpressions(finiteAutomaton);
        FileProcessor.of(filename)
                     .data(expressions)
                     .write();
    }

    /**
     * Calculates number of symbols in alphabet.
     *
     * @param dfa Finite automaton to be processed
     * @return Size of alphabet
     */
    private int getAlphabetSize(FiniteAutomaton<State, String> dfa) {
        return (int) dfa.getTransitions()
                        .stream()
                        .map(Transition::getSymbol)
                        .filter(symbol -> !symbol.isEmpty())
                        .distinct()
                        .count();
    }

    /**
     * Converts DFA to expressions.
     *
     * @param finiteAutomaton DFA to be converted
     * @return List of Expressions
     */
    private List<Expression> automatonToExpressions(FiniteAutomaton<State, String> finiteAutomaton) {
        Expression expression;

        if (finiteAutomaton.getAcceptStates().contains(finiteAutomaton.getStartState())) {
            expression = expressionMapper.asExpression(finiteAutomaton.getStartState(), "IF");
        } else {
            expression = expressionMapper.asExpression(finiteAutomaton.getStartState(), "I");
        }

        List<Expression> result = new LinkedList<>();
        result.add(new Expression(Integer.toString(finiteAutomaton.getStates().size())));
        result.add(new Expression(Integer.toString((getAlphabetSize(finiteAutomaton)))));
        result.add(expression);

        result.addAll(finiteAutomaton.getAcceptStates()
                                     .stream()
                                     .filter(state -> !state.equals(finiteAutomaton.getStartState()))
                                     .map(state -> expressionMapper.asExpression(state, "F"))
                                     .collect(toList()));

        result.addAll(finiteAutomaton.getStates()
                                     .stream()
                                     .filter(state -> !finiteAutomaton.getAcceptStates().contains(state))
                                     .filter(state -> !state.equals(finiteAutomaton.getStartState()))
                                     .map(state -> expressionMapper.asExpression(state, ""))
                                     .collect(toList()));

        result.addAll(finiteAutomaton.getTransitions()
                                     .stream()
                                     .map(Transition::getSymbol)
                                     .filter(symbol -> !symbol.isEmpty())
                                     .distinct()
                                     .map(Expression::new)
                                     .collect(toList()));

        result.addAll(finiteAutomaton.getTransitions()
                                     .stream()
                                     .map(expressionMapper::asExpression)
                                     .collect(toList()));

        return result;
    }
}
