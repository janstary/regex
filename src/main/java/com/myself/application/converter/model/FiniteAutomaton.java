package com.myself.application.converter.model;

import com.myself.application.converter.NoSuchTransitionException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;

/**
 * Class represents final automata.
 * This class is inspired by the mathematical representation of the finite automata:
 * M = (Q, Σ, δ, q₀, F) where:
 * <p>
 * Σ    - E(Actually, E represents only type of the alphabet, which is sufficient because all alphabet is defined in transitions.)
 * q₀   - startState
 * F    - acceptStates
 * δ    - transitionFunction(...)
 *
 * @param <Q> state type
 * @param <E> alphabet type
 */
public abstract class FiniteAutomaton<Q, E> {

    /**
     * Start state of the automata
     */
    protected final Q startState;
    /**
     * Represents all acceptable states of the automata
     */
    protected final Set<Q> acceptStates;
    /**
     * Represents transition table of the automata
     */
    protected final Set<Transition<Q, E>> transitions;
    /**
     * Represents all states of the finite automata
     */
    protected final Set<Q> states;

    public FiniteAutomaton(Q startState,
                           Set<Q> acceptStates,
                           Set<Transition<Q, E>> transitions,
                           Set<Q> states) {
        this.startState = startState;
        this.acceptStates = acceptStates;
        this.transitions = transitions;
        this.states = states;
    }

    public Q getStartState() {
        return startState;
    }

    public Set<Q> getAcceptStates() {
        return acceptStates;
    }

    public Set<Transition<Q, E>> getTransitions() {
        return transitions;
    }

    public Set<Q> getStates() {
        return states;
    }

    /**
     * Represents transition function of the finite automata. Based on state
     * and specific symbol, returns all achievable states by filtering all transitions.
     *
     * @param state  start state of the transition
     * @param symbol certain symbol
     * @return list of all achievable states
     * @throws NoSuchTransitionException if there is no such transition
     */
    public List<Q> transitionFunction(final Q state, final E symbol) throws NoSuchTransitionException {
        Predicate<Transition> predicate = (transition)
                -> transition.getFrom().equals(state) && transition.getSymbol().equals(symbol);

        Optional<List<Q>> result = of(this.transitions.stream()
                                                      .filter(predicate)
                                                      .map(Transition::getTo)
                                                      .collect(toList()));
        return result.orElseThrow(() -> new NoSuchTransitionException("No such transition."));
    }

    /**
     * Iterates through all symbols in input and tries to compute next state.
     * If last state is located in final states, final automata accepts the input.
     *
     * @param input certain string
     * @return true if finite automata accepts the input, or false if not
     */
    public abstract boolean accepts(final Iterable<E> input);
}
