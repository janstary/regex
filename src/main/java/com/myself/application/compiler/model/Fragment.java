package com.myself.application.compiler.model;

public enum Fragment {
    CHARACTER,
    EPSILON,
    CONCATENATION,
    UNION,
    ITERATION
}
