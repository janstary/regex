package com.myself.application.converter;

import com.myself.application.common.assertions.DFAAssert;
import com.myself.application.converter.impl.NFAtoDFAConverter;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.NFA;
import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Set.of;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(Alphanumeric.class)
public class NFAToDFAConverterTest {

    @Test
    void test_01_success() {
        // GIVEN
        List<State> states = createStates(1);

        NFA<State, String> nfa = new NFA<>(states.get(0),
                                           of(states.get(0)),
                                           of(new Transition<>("a", states.get(0), states.get(0)),
                                              new Transition<>("a", states.get(0), states.get(1)),
                                              new Transition<>("b", states.get(0), states.get(0)),
                                              new Transition<>("b", states.get(1), states.get(0)),
                                              new Transition<>("b", states.get(1), states.get(1))),
                                           new HashSet<>(states));

        // WHEN
        DFA<State, String> result = NFAtoDFAConverter.builder()
                                                     .nfa(nfa)
                                                     .build()
                                                     .convert();

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("a", "b", "a", "b")));
        DFAAssert.assertThat(result).hasStartState("q0")
                 .hasAcceptState("q0")
                 .hasAcceptState("q0q1")
                 .hasState("q0")
                 .hasState("q0q1")
                 .hasTransition("q0", "a", "q0q1")
                 .hasTransition("q0", "b", "q0")
                 .hasTransition("q0q1", "a", "q0q1")
                 .hasTransition("q0q1", "b", "q0q1")
                 .hasTransitionsSize(4)
                 .hasAcceptStatesSize(2)
                 .hasStatesSize(2);
    }

    @Test
    void test_02_success() {
        // GIVEN
        List<State> states = createStates(2);

        NFA<State, String> nfa = new NFA<>(states.get(0),
                                           of(states.get(0), states.get(2)),
                                           of(new Transition<>("a", states.get(0), states.get(1)),
                                              new Transition<>("", states.get(0), states.get(2)),
                                              new Transition<>("b", states.get(1), states.get(1)),
                                              new Transition<>("a", states.get(2), states.get(1)),
                                              new Transition<>("a", states.get(2), states.get(2))),
                                           new HashSet<>(states));

        // WHEN
        DFA<State, String> result = NFAtoDFAConverter.builder()
                                                     .nfa(nfa)
                                                     .build()
                                                     .convert();

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("a", "a", "a", "a", "a")));
        assertFalse(result.accepts(asList("a", "a", "b", "a", "a")));
        DFAAssert.assertThat(result).hasStartState("q0q2")
                 .hasAcceptState("q0q2")
                 .hasAcceptState("q1q2")
                 .hasState("q0q2")
                 .hasState("q1q2")
                 .hasState("q1")
                 .hasTransition("q0q2", "a", "q1q2")
                 .hasTransition("q1q2", "a", "q1q2")
                 .hasTransition("q1q2", "b", "q1")
                 .hasTransition("q1", "b", "q1")
                 .hasTransitionsSize(4)
                 .hasAcceptStatesSize(2)
                 .hasStatesSize(3);
    }

    @Test
    void test_03_success() {
        // GIVEN
        List<State> states = createStates(2);
        State statef = new State("qf");
        states.add(statef);

        NFA<State, String> nfa = new NFA<>(states.get(0),
                                           of(states.get(2), statef),
                                           of(new Transition<>("a", states.get(0), states.get(1)),
                                              new Transition<>("", states.get(0), statef),
                                              new Transition<>("a", states.get(1), states.get(1)),
                                              new Transition<>("a", states.get(1), states.get(2)),
                                              new Transition<>("b", states.get(1), statef),
                                              new Transition<>("a", states.get(2), states.get(2)),
                                              new Transition<>("b", states.get(2), statef)),
                                           new HashSet<>(states));

        // WHEN
        DFA<State, String> result = NFAtoDFAConverter.builder()
                                                     .nfa(nfa)
                                                     .build()
                                                     .convert();

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("a", "a", "a", "a", "b")));
        assertFalse(result.accepts(asList("a", "a", "b", "b", "a")));
        DFAAssert.assertThat(result).hasStartState("q0qf")
                 .hasAcceptState("q0qf")
                 .hasAcceptState("q1q2")
                 .hasAcceptState("qf")
                 .hasState("q0qf")
                 .hasState("q1q2")
                 .hasState("q1")
                 .hasState("qf")
                 .hasTransition("q0qf", "a", "q1")
                 .hasTransition("q1", "a", "q1q2")
                 .hasTransition("q1", "b", "qf")
                 .hasTransition("q1q2", "a", "q1q2")
                 .hasTransition("q1q2", "b", "qf")
                 .hasTransitionsSize(5)
                 .hasAcceptStatesSize(3)
                 .hasStatesSize(4);
    }

    @Test
    void test_04_success() {
        // GIVEN
        List<State> states = createStates(10);

        NFA<State, String> nfa = new NFA<>(states.get(0),
                                           of(states.get(5), states.get(9), states.get(10)),
                                           of(new Transition<>("", states.get(0), states.get(1)),
                                              new Transition<>("", states.get(0), states.get(3)),
                                              new Transition<>("c", states.get(1), states.get(2)),
                                              new Transition<>("", states.get(2), states.get(1)),
                                              new Transition<>("", states.get(2), states.get(3)),
                                              new Transition<>("a", states.get(3), states.get(4)),
                                              new Transition<>("", states.get(4), states.get(5)),
                                              new Transition<>("", states.get(5), states.get(6)),
                                              new Transition<>("", states.get(6), states.get(7)),
                                              new Transition<>("", states.get(6), states.get(8)),
                                              new Transition<>("b", states.get(7), states.get(9)),
                                              new Transition<>("c", states.get(8), states.get(10)),
                                              new Transition<>("", states.get(9), states.get(6)),
                                              new Transition<>("", states.get(10), states.get(6))),
                                           new HashSet<>(states));

        // WHEN
        DFA<State, String> result = NFAtoDFAConverter.builder()
                                                     .nfa(nfa)
                                                     .build()
                                                     .convert();

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("c", "c", "a", "c", "b")));
        assertFalse(result.accepts(asList("c", "c", "c", "c", "b")));
        DFAAssert.assertThat(result).hasStartState("q0q1q3")
                 .hasAcceptState("q4q5q6q7q8")
                 .hasAcceptState("q10q6q7q8")
                 .hasAcceptState("q6q7q8q9")
                 .hasState("q0q1q3")
                 .hasState("q1q2q3")
                 .hasState("q4q5q6q7q8")
                 .hasState("q10q6q7q8")
                 .hasState("q6q7q8q9")
                 .hasTransition("q0q1q3", "a", "q4q5q6q7q8")
                 .hasTransition("q0q1q3", "c", "q1q2q3")
                 .hasTransition("q1q2q3", "a", "q4q5q6q7q8")
                 .hasTransition("q1q2q3", "c", "q1q2q3")
                 .hasTransition("q4q5q6q7q8", "b", "q6q7q8q9")
                 .hasTransition("q4q5q6q7q8", "c", "q10q6q7q8")
                 .hasTransition("q10q6q7q8", "b", "q6q7q8q9")
                 .hasTransition("q10q6q7q8", "c", "q10q6q7q8")
                 .hasTransition("q6q7q8q9", "b", "q6q7q8q9")
                 .hasTransition("q6q7q8q9", "c", "q10q6q7q8")
                 .hasTransitionsSize(10)
                 .hasAcceptStatesSize(3)
                 .hasStatesSize(5);
    }

    @Test
    void test_05_success() {
        // GIVEN
        List<State> states = createStates(16);

        NFA<State, String> nfa = new NFA<>(states.get(1),
                                           of(states.get(14), states.get(16)),
                                           of(new Transition<>("", states.get(1), states.get(2)),
                                              new Transition<>("", states.get(1), states.get(7)),
                                              new Transition<>("", states.get(2), states.get(3)),
                                              new Transition<>("", states.get(2), states.get(12)),
                                              new Transition<>("b", states.get(3), states.get(4)),
                                              new Transition<>("", states.get(4), states.get(5)),
                                              new Transition<>("a", states.get(5), states.get(6)),
                                              new Transition<>("", states.get(6), states.get(3)),
                                              new Transition<>("", states.get(6), states.get(12)),
                                              new Transition<>("", states.get(7), states.get(8)),
                                              new Transition<>("", states.get(7), states.get(12)),
                                              new Transition<>("c", states.get(8), states.get(9)),
                                              new Transition<>("", states.get(9), states.get(10)),
                                              new Transition<>("a", states.get(10), states.get(11)),
                                              new Transition<>("", states.get(11), states.get(12)),
                                              new Transition<>("", states.get(11), states.get(8)),
                                              new Transition<>("b", states.get(12), states.get(13)),
                                              new Transition<>("", states.get(13), states.get(14)),
                                              new Transition<>("", states.get(14), states.get(15)),
                                              new Transition<>("b", states.get(15), states.get(16)),
                                              new Transition<>("", states.get(16), states.get(15))),
                                           new HashSet<>(states.subList(1, states.size())));

        // WHEN
        DFA<State, String> result = NFAtoDFAConverter.builder()
                                                     .nfa(nfa)
                                                     .build()
                                                     .convert();

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("b", "a", "b", "a", "b")));
        assertFalse(result.accepts(asList("b", "a", "c", "a", "b")));
        DFAAssert.assertThat(result).hasStartState("q1q12q2q3q7q8")
                 .hasAcceptState("q13q14q15q4q5")
                 .hasAcceptState("q13q14q15")
                 .hasAcceptState("q15q16")
                 .hasState("q1q12q2q3q7q8")
                 .hasState("q13q14q15q4q5")
                 .hasState("q13q14q15")
                 .hasState("q10q9")
                 .hasState("q12q3q6")
                 .hasState("q11q12q8")
                 .hasState("q15q16")
                 .hasTransition("q1q12q2q3q7q8", "b", "q13q14q15q4q5")
                 .hasTransition("q1q12q2q3q7q8", "c", "q10q9")
                 .hasTransition("q13q14q15q4q5", "a", "q12q3q6")
                 .hasTransition("q13q14q15q4q5", "b", "q15q16")
                 .hasTransition("q10q9", "a", "q11q12q8")
                 .hasTransition("q12q3q6", "b", "q13q14q15q4q5")
                 .hasTransition("q15q16", "b", "q15q16")
                 .hasTransition("q11q12q8", "b", "q13q14q15")
                 .hasTransition("q11q12q8", "c", "q10q9")
                 .hasTransition("q13q14q15", "b", "q15q16")
                 .hasTransitionsSize(10)
                 .hasAcceptStatesSize(3)
                 .hasStatesSize(7);
    }

    /**
     * Creates State instances from 0  to index.
     *
     * @param index index of last element
     * @return List of created States
     */
    private List<State> createStates(int index) {
        List<State> states = new LinkedList<>();

        for (int i = 0; i <= index; i++) {
            states.add(new State(format("q%s", i)));
        }

        return states;
    }

}
