package com.myself.application.regex.creator.model;

import java.util.Arrays;
import java.util.Objects;

/**
 * Class represents
 */
public class Expression {

    /**
     * Type of expression
     */
    private final ExpressionType type;
    /**
     * Parsed value of one line
     */
    private final String[] value;

    public Expression(ExpressionType type, String[] value) {
        this.type = type;
        this.value = value;
    }

    public ExpressionType getType() {
        return type;
    }

    public String[] getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Expression that = (Expression) o;
        return type == that.type &&
               Arrays.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(type);
        result = 31 * result + Arrays.hashCode(value);
        return result;
    }
}
