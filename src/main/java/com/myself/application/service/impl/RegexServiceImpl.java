package com.myself.application.service.impl;

import com.myself.application.compiler.model.Description;
import com.myself.application.mapping.DefinitionMapper;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.regex.impl.DefaultRegexCreator;
import com.myself.application.service.RegexService;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class RegexServiceImpl implements RegexService {

    private final DefinitionMapper definitionMapper;

    public RegexServiceImpl(DefinitionMapper definitionMapper) {
        this.definitionMapper = requireNonNull(definitionMapper,
                                               "Description mapper should be provided.");
    }

    @Override
    public FiniteAutomaton createFA(List<Description> descriptions) {
        return DefaultRegexCreator.builder()
                                  .definitions(definitionMapper.mapList(descriptions,
                                                                        definitionMapper::asDefinition))
                                  .build()
                                  .create();
    }
}
