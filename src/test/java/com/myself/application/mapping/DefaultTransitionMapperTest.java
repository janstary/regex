package com.myself.application.mapping;

import com.myself.application.common.assertions.TransitionAssert;
import com.myself.application.mapping.impl.DefaultStateMapper;
import com.myself.application.mapping.impl.DefaultTransitionMapper;
import com.myself.application.regex.creator.model.State;
import com.myself.application.regex.creator.model.Transition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class DefaultTransitionMapperTest {

    private TransitionMapper transitionMapper;

    @BeforeEach
    void setUp() {
        StateMapper stateMapper = new DefaultStateMapper();
        this.transitionMapper = new DefaultTransitionMapper(stateMapper);
    }

    @Test
    void test_01_as_transition() {
        // GIVEN
        Transition transition = new Transition("a",
                                               new State("q0", true, true),
                                               new State("q1", true, false));

        // WHEN
        com.myself.application.converter.model.Transition<
                com.myself.application.converter.model.State, String> result = this.transitionMapper.asTransition(transition);

        // THEN
        assertNotNull(result);
        TransitionAssert.assertThat(result).hasFrom("q0")
                        .hasTo("q1")
                        .hasSymbol("a");
    }

}
