package com.myself.application.converter.model;

import java.util.Set;

/**
 * Class represents deterministic implementation of final automata.
 *
 * @param <Q> state type
 * @param <E> alphabet type
 */
public class DFA<Q, E> extends FiniteAutomaton<Q, E> {

    public DFA(Q startState,
               Set<Q> acceptStates,
               Set<Transition<Q, E>> transitions,
               Set<Q> states) {
        super(startState, acceptStates, transitions, states);
    }

    @Override
    public boolean accepts(final Iterable<E> input) {
        Q state = this.startState;

        try {
            for (E symbol : input) {
                state = this.transitionFunction(state, symbol).get(0);
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }

        return this.acceptStates.contains(state);
    }
}
