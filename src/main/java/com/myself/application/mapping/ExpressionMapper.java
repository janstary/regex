package com.myself.application.mapping;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import com.myself.application.regex.creator.model.Expression;

/**
 * Maps Expressions entities.
 */
public interface ExpressionMapper extends CollectionMapper {

    /**
     * Converts Description from processor package to Description from creator package.
     *
     * @param input Description to be converted
     * @return Mapped Description
     */
    Expression asExpression(com.myself.application.processor.model.Expression input);

    /**
     * Converts State from converter package to Description from processor package.
     *
     * @param state  State to be converted
     * @param symbol
     * @return Mapped Description
     */
    com.myself.application.processor.model.Expression asExpression(State state, String symbol);

    /**
     * Converts Transition from converter package to Description from processor package.
     *
     * @param transition Transition to be converted
     * @return Mapped Description
     */
    com.myself.application.processor.model.Expression asExpression(Transition<State, String> transition);

}
