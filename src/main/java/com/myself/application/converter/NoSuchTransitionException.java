package com.myself.application.converter;

/**
 * Exception is thrown when the transition was not found.
 */
public class NoSuchTransitionException extends RuntimeException {

    public NoSuchTransitionException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchTransitionException(String message) {
        super(message);
    }
}
