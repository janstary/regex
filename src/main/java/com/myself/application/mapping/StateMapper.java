package com.myself.application.mapping;

import com.myself.application.converter.model.State;

/**
 * Maps state from creator package to state from converter
 */
public interface StateMapper extends CollectionMapper {

    /**
     * Takes input of one specific type and produce output of another specific type
     *
     * @param input entity to be mapped
     * @return Mapped entity
     */
    State asState(com.myself.application.regex.creator.model.State input);

}
