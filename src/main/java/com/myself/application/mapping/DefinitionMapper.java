package com.myself.application.mapping;

import com.myself.application.compiler.model.Description;
import com.myself.application.processor.model.Expression;
import com.myself.application.regex.model.Definition;

public interface DefinitionMapper extends CollectionMapper {

    /**
     * Maps the Expression from processor package to Description from regex package by specific rules:
     * <p>
     * Empty symbol - represents NFA which accepts epsilon symbol
     * Specific symbol - represents NFA which accepts exactly this one specific symbol
     * U,i,j - represents UNION operation of NFA`s from i-th and j-th lines
     * C,i,j - represents CONCATENATION operation of NFA`s from i-th and j-th lines
     * I,i - represents ITERATION operation of NFA from i-th
     */
    Definition asDefinition(Expression input);

    /**
     *
     * @param input
     * @return
     */
    Definition asDefinition(Description input);

}
