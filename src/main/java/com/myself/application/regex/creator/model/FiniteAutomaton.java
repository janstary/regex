package com.myself.application.regex.creator.model;

import java.util.Objects;
import java.util.Set;

/**
 * Class represents final automaton.
 */
public class FiniteAutomaton {

    /**
     * Start state of the automaton
     */
    private final State startState;
    /**
     * Represents all acceptable states of the automaton
     */
    private final Set<State> acceptStates;
    /**
     * Represents transition table of the automaton
     */
    private final Set<Transition> transitions;
    /**
     * All states
     */
    private final Set<State> states;

    public FiniteAutomaton(State startState,
                           Set<State> acceptStates,
                           Set<Transition> transitions,
                           Set<State> states) {
        this.startState = startState;
        this.acceptStates = acceptStates;
        this.transitions = transitions;
        this.states = states;
    }

    public State getStartState() {
        return startState;
    }

    public Set<State> getAcceptStates() {
        return acceptStates;
    }

    public Set<Transition> getTransitions() {
        return transitions;
    }

    public Set<State> getStates() {
        return states;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FiniteAutomaton automaton = (FiniteAutomaton) o;
        return Objects.equals(startState, automaton.startState) &&
               Objects.equals(acceptStates, automaton.acceptStates) &&
               Objects.equals(transitions, automaton.transitions) &&
               Objects.equals(states, automaton.states);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startState, acceptStates, transitions, states);
    }
}
