package com.myself.application.converter.model;

import java.util.Set;

/**
 * Class represents non-deterministic implementation of final automata.
 *
 * @param <Q> state type
 * @param <E> alphabet type
 */
public class NFA<Q, E> extends FiniteAutomaton<Q, E> {

    public NFA(Q startState,
               Set<Q> acceptStates,
               Set<Transition<Q, E>> transitions,
               Set<Q> states) {
        super(startState, acceptStates, transitions, states);
    }

    @Override
    public boolean accepts(final Iterable<E> input) {
        return false;
    }

}
