package com.myself.application.service;

import com.myself.application.compiler.Compiler;
import com.myself.application.compiler.DescriptionFactory;
import com.myself.application.compiler.Lexer;
import com.myself.application.compiler.Parser;
import com.myself.application.compiler.TokenFactory;
import com.myself.application.compiler.impl.DefaultDescriptionFactory;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.impl.RegexCompiler;
import com.myself.application.compiler.impl.RegexLexer;
import com.myself.application.compiler.impl.RegexParser;
import com.myself.application.compiler.model.Description;
import com.myself.application.converter.model.DFA;
import com.myself.application.converter.model.State;
import com.myself.application.mapping.AutomatonMapper;
import com.myself.application.mapping.DefinitionMapper;
import com.myself.application.mapping.ExpressionMapper;
import com.myself.application.mapping.FragmentMapper;
import com.myself.application.mapping.StateMapper;
import com.myself.application.mapping.TransitionMapper;
import com.myself.application.mapping.impl.DefaultAutomatonMapper;
import com.myself.application.mapping.impl.DefaultDefinitionMapper;
import com.myself.application.mapping.impl.DefaultExpressionMapper;
import com.myself.application.mapping.impl.DefaultFragmentMapper;
import com.myself.application.mapping.impl.DefaultStateMapper;
import com.myself.application.mapping.impl.DefaultTransitionMapper;
import com.myself.application.processor.model.Expression;
import com.myself.application.processor.model.ProcessorResult;
import com.myself.application.regex.creator.model.FiniteAutomaton;
import com.myself.application.service.impl.CompilerServiceImpl;
import com.myself.application.service.impl.ConverterServiceImpl;
import com.myself.application.service.impl.ProcessorServiceImpl;
import com.myself.application.service.impl.RegexServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.Mock;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(Alphanumeric.class)
public class ConverterServiceImplTest {

    @Mock
    private DefinitionMapper definitionMapper;

    @Mock
    private ExpressionMapper expressionMapper;

    @Mock
    private RegexService regexService;

    @Mock
    private ProcessorService processorService;

    @Mock
    private CompilerService compilerService;

    @Mock
    private AutomatonMapper<State, String> automatonMapper;

    private ConverterService converterService;

    @BeforeEach
    void setUp() {
        StateMapper stateMapper = new DefaultStateMapper();
        TransitionMapper transitionMapper = new DefaultTransitionMapper(stateMapper);
        FragmentMapper fragmentMapper = new DefaultFragmentMapper();

        TokenFactory tokenFactory = new DefaultTokenFactory();
        DescriptionFactory descriptionFactory = new DefaultDescriptionFactory();
        Lexer lexer = new RegexLexer(tokenFactory);
        Parser parser = new RegexParser();
        Compiler compiler = new RegexCompiler(lexer, parser, descriptionFactory);

        automatonMapper = new DefaultAutomatonMapper(stateMapper, transitionMapper);
        definitionMapper = new DefaultDefinitionMapper(fragmentMapper);
        expressionMapper = new DefaultExpressionMapper();
        processorService = new ProcessorServiceImpl(expressionMapper);
        compilerService = new CompilerServiceImpl(compiler);
        regexService = new RegexServiceImpl(definitionMapper);
        converterService = new ConverterServiceImpl();
    }

    @Test
    void test_01_read_file_and_convert_nfa_to_dfa() {
        // GIVEN
        String filename = "src\\test\\resources\\data1.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));
        FiniteAutomaton automaton = regexService.createFA(descriptions);

        // WHEN
        DFA<State, String> result = converterService.convertNFAToDFA(automatonMapper.asNFA(automaton));

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("a", "a", "a", "a", "b")));
        assertFalse(result.accepts(asList("a", "a", "b", "a", "b")));
    }

    @Test
    void test_02_read_file_and_convert_nfa_to_dfa() {
        // GIVEN
        String filename = "src\\test\\resources\\data2.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));
        FiniteAutomaton automaton = regexService.createFA(descriptions);

        // WHEN
        DFA<State, String> result = converterService.convertNFAToDFA(automatonMapper.asNFA(automaton));

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("b", "a", "b", "a", "c", "b", "b", "b")));
        assertFalse(result.accepts(asList("b", "a", "b", "a", "c", "c", "b", "b")));
    }

    @Test
    void test_03_read_file_and_convert_nfa_to_dfa() {
        // GIVEN
        String filename = "src\\test\\resources\\data3.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));
        FiniteAutomaton automaton = regexService.createFA(descriptions);

        // WHEN
        DFA<State, String> result = converterService.convertNFAToDFA(automatonMapper.asNFA(automaton));

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("b", "a")));
        assertFalse(result.accepts(singletonList("a")));
    }

    @Test
    void test_04_read_file_and_convert_nfa_to_dfa() {
        // GIVEN
        String filename = "src\\test\\resources\\data4.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));
        FiniteAutomaton automaton = regexService.createFA(descriptions);

        // WHEN
        DFA<State, String> result = converterService.convertNFAToDFA(automatonMapper.asNFA(automaton));

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("A", "b", "a", "c", "B")));
        assertFalse(result.accepts(asList("A", "B", "a", "c", "B")));
    }

    @Test
    void test_05_read_file_and_convert_nfa_to_dfa() {
        // GIVEN
        String filename = "src\\test\\resources\\data5.txt";
        ProcessorResult<Expression> processorResult = processorService.readAutomatonFromFile(filename);
        List<Description> descriptions = compilerService.compile(processorResult.getData().get(0));
        FiniteAutomaton automaton = regexService.createFA(descriptions);

        // WHEN
        DFA<State, String> result = converterService.convertNFAToDFA(automatonMapper.asNFA(automaton));

        // THEN
        assertNotNull(result);
        assertTrue(result.accepts(asList("E", "n", "d")));
        assertFalse(result.accepts(asList("e", "n", "D")));
    }

}
