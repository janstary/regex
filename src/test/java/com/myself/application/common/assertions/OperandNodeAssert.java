package com.myself.application.common.assertions;

import com.myself.application.compiler.model.OperandNode;
import org.assertj.core.api.AbstractAssert;

public class OperandNodeAssert extends AbstractAssert<OperandNodeAssert, OperandNode> {

    private OperandNodeAssert(OperandNode actual) {
        super(actual, OperandNodeAssert.class);
    }

    public static OperandNodeAssert assertThat(OperandNode actual) {
        return new OperandNodeAssert(actual);
    }

    public OperandNodeAssert hasNode(String root, String left, String right) {
        isNotNull();

//        if (!Objects.equals(actual., fragment)) {
//            failWithMessage("Expected definition to have fragment <%s> but had <%s>.",
//                            fragment,
//                            actual.getFragment());
//        }
        return this;
    }

}
