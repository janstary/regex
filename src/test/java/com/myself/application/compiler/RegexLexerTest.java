package com.myself.application.compiler;

import com.myself.application.common.assertions.TokenListAssert;
import com.myself.application.compiler.impl.DefaultTokenFactory;
import com.myself.application.compiler.impl.RegexLexer;
import com.myself.application.compiler.model.Token;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.Alphanumeric;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.List;

import static com.myself.application.compiler.model.TokenType.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(Alphanumeric.class)
public class RegexLexerTest {

    private RegexLexer lexer;

    @BeforeEach
    void setUp() {
        TokenFactory factory = new DefaultTokenFactory();
        this.lexer = new RegexLexer(factory);
    }

    @Test
    void test_01_success() {
        // GIVEN
        String regex = "ab";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(1, CONCATENATION, ".")
                       .hasTypeAndSymbol(2, CHARACTER_AUTOMATON, "b");
    }

    @Test
    void test_02_success() {
        // GIVEN
        String regex = "a|b";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(1, UNION, "|")
                       .hasTypeAndSymbol(2, CHARACTER_AUTOMATON, "b");
    }

    @Test
    void test_03_success() {
        // GIVEN
        String regex = "(a|b)*";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(1, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(2, UNION, "|")
                       .hasTypeAndSymbol(3, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(4, RIGHT_BRACKET, ")")
                       .hasTypeAndSymbol(5, ITERATION, "*");
    }

    @Test
    void test_04_success() {
        // GIVEN
        String regex = "(a|b)*(ab)";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(1, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(2, UNION, "|")
                       .hasTypeAndSymbol(3, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(4, RIGHT_BRACKET, ")")
                       .hasTypeAndSymbol(5, ITERATION, "*")
                       .hasTypeAndSymbol(6, CONCATENATION, ".")
                       .hasTypeAndSymbol(7, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(8, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(9, CONCATENATION, ".")
                       .hasTypeAndSymbol(10, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(11, RIGHT_BRACKET, ")");
    }

    @Test
    void test_05_success() {
        // GIVEN
        String regex = "abcd";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(1, CONCATENATION, ".")
                       .hasTypeAndSymbol(2, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(3, CONCATENATION, ".")
                       .hasTypeAndSymbol(4, CHARACTER_AUTOMATON, "c")
                       .hasTypeAndSymbol(5, CONCATENATION, ".")
                       .hasTypeAndSymbol(6, CHARACTER_AUTOMATON, "d");
    }

    @Test
    void test_06_success() {
        // GIVEN
        String regex = "(a|b)(ab)";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(1, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(2, UNION, "|")
                       .hasTypeAndSymbol(3, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(4, RIGHT_BRACKET, ")")
                       .hasTypeAndSymbol(5, CONCATENATION, ".")
                       .hasTypeAndSymbol(6, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(7, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(8, CONCATENATION, ".")
                       .hasTypeAndSymbol(9, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(10, RIGHT_BRACKET, ")");
    }

    @Test
    void test_07_success() {
        // GIVEN
        String regex = "(a|b)a";

        // WHEN
        List<Token> result = lexer.tokenize(regex);

        // THEN
        assertNotNull(regex);
        TokenListAssert.assertThat(result)
                       .hasTypeAndSymbol(0, LEFT_BRACKET, "(")
                       .hasTypeAndSymbol(1, CHARACTER_AUTOMATON, "a")
                       .hasTypeAndSymbol(2, UNION, "|")
                       .hasTypeAndSymbol(3, CHARACTER_AUTOMATON, "b")
                       .hasTypeAndSymbol(4, RIGHT_BRACKET, ")")
                       .hasTypeAndSymbol(5, CONCATENATION, ".")
                       .hasTypeAndSymbol(6, CHARACTER_AUTOMATON, "a");
    }

}
