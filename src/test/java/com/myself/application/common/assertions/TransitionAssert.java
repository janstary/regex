package com.myself.application.common.assertions;

import com.myself.application.converter.model.State;
import com.myself.application.converter.model.Transition;
import org.assertj.core.api.AbstractAssert;

import java.util.Objects;

public class TransitionAssert extends AbstractAssert<TransitionAssert, Transition<State, String>> {

    private TransitionAssert(Transition<State, String> actual) {
        super(actual, TransitionAssert.class);
    }

    public static TransitionAssert assertThat(Transition<State, String> actual) {
        return new TransitionAssert(actual);
    }

    public TransitionAssert hasFrom(String from) {
        isNotNull();

        if (!Objects.equals(actual.getFrom().getName(), from)) {
            failWithMessage("Expected transition to have from state <%s> but had <%s>.",
                            from,
                            actual.getFrom().getName());
        }
        return this;
    }

    public TransitionAssert hasTo(String from) {
        isNotNull();

        if (!Objects.equals(actual.getTo().getName(), from)) {
            failWithMessage("Expected transition to have to state <%s> but had <%s>.",
                            from,
                            actual.getTo().getName());
        }
        return this;
    }

    public TransitionAssert hasSymbol(String symbol) {
        isNotNull();

        if (!Objects.equals(actual.getSymbol(), symbol)) {
            failWithMessage("Expected transition to have symbol <%s> but had <%s>.",
                            symbol,
                            actual.getFrom().getName());
        }
        return this;
    }

}
